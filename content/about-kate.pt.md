---
author: Christoph Cullmann
date: 2010-07-09 08:40:19
title: "Caracter\xEDsticas"
---
## Funcionalidades da Aplicação

![Imagem que mostra a funcionalidade de janelas divididas do Kate e o 'plugin' do terminal](/images/kate-window.png)

* Veja e edite vários documentos ao mesmo tempo, dividindo a janela na horizontal e na vertical

* Diversos 'plugins': [Terminal incorporado](https://konsole.kde.org), 'plugin' de SQL, 'plugin' de compilação, 'plugin' do GDB, Substituição nos Ficheiros, entre outros

* Interface multi-documentos (MDI)

* Suporte para sessões

## Funcionalidades Gerais

![Imagem que mostra a funcionalidade de pesquisa e substituição do Kate](/images/kate-search-replace.png)

* Suporte para diferentes codificações (Unicode, entre muitas outras)

* Suporte para o desenho de texto bidireccional

* Suporte para diferentes fins de linha (Windows, Unix, Mac), incluindo a detecção automática

* Transparência na rede (abertura de ficheiros remotos)

* Extensível através de programação

## Funcionalidades Avançadas do Editor

![Imagem do contorno do Kate com o número da linha e os favoritos](/images/kate-border.png)

* Sistema de favoritos (também suportado: pontos de paragem, etc.)

* Marcações na barra de deslocamento

* Indicadores de linhas modificadas

* Números de linha

* Dobragem de código

## Realce de Sintaxe

![Imagem das funcionalidades de realce de sintaxe do Kate](/images/kate-syntax.png)

* Suporte para o realce de sintaxe de cerca de 300 linguagens

* Correspondência de parêntesis

* Verificação ortográfica inteligente e em tempo-real

* Realce das palavras seleccionadas

## Funcionalidades de Programação

![Imagem das funcionalidades de programação do Kate](/images/kate-programming.png)

* Indentação automática programável

* Tratamento da aplicação e remoção de comentários inteligente

* Completação automática com sugestão dos argumentos

* Modo de introdução de dados do VI

* Modo de selecção em blocos rectangulares

## Pesquisa & Substituição

![Imagem da funcionalidade de pesquisa incremental do Kate](/images/kate-search.png)

* Pesquisa incremental, também conhecida por &#8220;pesquisa à medida que escreve&#8221;

* Suporte para a pesquisa & substituição em várias linhas

* Suporte para expressões regulares

* Pesquisa & substituição em vários ficheiros abertos ou em ficheiros no disco

## Salvaguarda e Reposição

![Imagem do Kate a recuperar de um estoiro](/images/kate-crash.png)

* Cópias de segurança na gravação

* Ficheiros temporários para recuperar os dados em caso de estoiro do sistema

* Suporte para desfazer / refazer