---
author: Christoph Cullmann
date: 2010-07-09 14:40:05
title: Pridobi Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux in unixi

* Namesti Kate/KWrite [iz vaše distribucije]http://www.kde.org/download/distributions.php).

* [Build it](/build-it/#linux) iz vira.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

* [Kate v trgovini Windows Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

+ [Kate via Chocolatey](https://chocolatey.org/packages/kate)

* [Kate izdaja (64bit) namestilnik](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/)

* [Kate nočni (64bit) namestilnik](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/)

* [Izgradi ga](/build-it/#windows) iz vira.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/OS_X_El_Capitan_logo.svg_.png" >}}

### macOS

+ [Kate release installer](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/)

+ [Kate nightly installer](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/)

+ [Build it](/build-it/#mac) from source.