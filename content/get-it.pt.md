---
author: Christoph Cullmann
date: 2010-07-09 14:40:05
title: Obter o Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux & Unix'es

* Instale o Kate/KWrite [a partir da sua distribuição](http://www.kde.org/download/distributions.php).

* [Compile-o](/build-it/#linux) a partir do código.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Janelas

* [O Kate na Windows Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

* [O Kate no Chocolatey](https://chocolatey.org/packages/kate)

* [Instalador do Kate (versão a 64bits)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/)

* [Instalador do Kate (versão diária a 64 bits)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/)

* [Compile-o](/build-it/#windows) a partir do código.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/OS_X_El_Capitan_logo.svg_.png" >}}

### macOS

* [Instalador da versão do Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/)

* [Instalador da versão diária do Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/)

* [Compile-o](/build-it/#mac) a partir do código.