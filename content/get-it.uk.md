---
author: Christoph Cullmann
date: 2010-07-09 14:40:05
title: "\u041E\u0442\u0440\u0438\u043C\u0430\u0442\u0438 Kate"
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux та UNIX-и

* Встановіть Kate/KWrite [зі сховищ вашого дистрибутива](http://www.kde.org/download/distributions.php).

* [Зберіть програму](/build-it/#linux) з початкових кодів.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

* [Kate у Windows Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

* [Kate у Chocolatey](https://chocolatey.org/packages/kate)

* [Засіб встановлення випусків Kate (64-бітовий)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/)

* [Засіб встановлення щоденної версії Kate (64-бітовий)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/)

* [Зберіть програму](/build-it/#windows) з початкових кодів.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/OS_X_El_Capitan_logo.svg_.png" >}}

### macOS

* [Засіб встановлення випуску Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/)

* [Засіб встановлення щоденної версії Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/)

* [Зберіть програму](/build-it/#mac) з початкових кодів.