---
layout: index
---
Kate je sočasni urejevalnik več dokumentov, kot del [KDE](https://kde.org) od izdaje 2.2. Kate je bil aplikacija [KDE](https://kde.org/applications), ki omogoča preglednost omrežja in integracijo z izjemnimimožnostmi KDE. Izberite ga za ogled izvirne kode HTML iz konquerorja, urejanje nastavitvenih datotek, pisanje novih aplikacij ali katero koli drugo nalogo za urejanje besedila. Še vedno potrebujete samo en pojavek Kate. [Naučite se več...](/about/)

![Posnetek zaslona Kate, ki prikazuje več dokumentov in emulator
terminala](/images/kate-window.png)