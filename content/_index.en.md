---
layout: index # Don't translated this string
---

Kate is a multi-document editor part of [KDE](https://kde.org) since release 2.2. Being a [KDE applications](https://kde.org/applications), Kate ships with network transparency, as well as integration with the outstanding features of KDE. Choose it for viewing HTML sources from konqueror, editing configuration files, writing new applications or any other text editing task. You still need just one running instance of Kate. [Learn more...](/about/)

![Screenshot of Kate showing multiple documents and the building terminal emulator](/images/kate-window.png)
