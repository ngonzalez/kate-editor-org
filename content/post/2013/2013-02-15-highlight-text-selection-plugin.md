---
title: Highlight Text Selection Plugin
author: Dominik Haumann

date: 2013-02-15T08:42:18+00:00
url: /2013/02/15/highlight-text-selection-plugin/
pw_single_layout:
  - "1"
categories:
  - Users
tags:
  - planet

---
As a quick notice: The [highlight selection plugin][1] was not removed in KDE SC 4.10.0. Instead, a silly bug results in not loading the plugin. This is fixed for DKE 4.10.1. If you cannot wait, you can find a <a title="Bug report: Highlight Selected Text is missing" href="https://bugs.kde.org/show_bug.cgi?id=314530" target="_blank">workaround here</a> :-)

 [1]: /2010/11/14/highlight-selected-text/ "Highlight Selected Text"