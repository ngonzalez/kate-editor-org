---
title: 'Kate: Search & Replace Notifications in KDE 4.11'
author: Dominik Haumann

date: 2013-04-02T19:31:50+00:00
url: /2013/04/02/kate-search-replace-highlighting-in-kde-4-11/
pw_single_layout:
  - "1"
categories:
  - Developers
  - Users
tags:
  - planet

---
In KDE 4.10, the &#8220;Find All&#8221; and &#8220;Replace All&#8221; highlights all matches and at the same time shows [a passive notification in a bar below the view][1]. This bar is animated, and takes quite a lot of place in addition to the search & replace bar.

Since some days, Kate Part can also show passive notifications floating in the view. Hence, we&#8217;ve changed the passive notification to appear on the bottom right as a small info message, showing the number of matches. However, in order to make this passive notification as small as possible, we removed the &#8220;Close&#8221; button, since the notification is hidden after 3 seconds anyway. Further, we removed the &#8220;Keep Highlighting&#8221; button. If you want to keep the highlights, just do not close the search & replace bar. The following video demonstrates this behavior, first for KDE 4.10, then how it currently will be in KDE 4.11 (<a title="Kate: Search & Replace Notifications in KDE 4.11" href="https://www.youtube.com/watch?v=7kDLd7bYJkA&hd=1" target="_blank">watch the video in 720p</a>):

<p style="text-align: center;">
</p>

 [1]: /2012/11/06/passive-notifications-in-kate-part/ "Kate Notifications in KDE 4.10"