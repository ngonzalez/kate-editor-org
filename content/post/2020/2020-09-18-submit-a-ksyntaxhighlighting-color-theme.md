---
title: Submit a KSyntaxHighlighting Color Theme
author: Christoph Cullmann
date: 2020-09-18T21:50:00+02:00
url: /post/2020/2020-09-18-submit-a-ksyntaxhighlighting-color-theme
---

### Color theme? What?

The [KSyntaxHighlighting framework](https://api.kde.org/frameworks/syntax-highlighting/html/index.html) provides support for color themes.

These color themes specify all colors (text/background/selection/...) and font style roles (italic/bold/...) that are used for the highlighting.
The definition happens in some easy to understand JSON file format.


Starting with the upcoming [KDE Frameworks 5.75 release](/post/2020/2020-09-13-kate-color-themes-5.75/), all [KTextEditor framework](https://api.kde.org/frameworks/ktexteditor/html/) based application will support these color themes for their embedded editors.

This includes Kate & KWrite, but naturally a lot more, like for example [KDevelop](https://kdevelop.org), [Kile](https://kile.sourceforge.io/) and [RKWard](https://rkward.kde.org/).

Beside using the themes, all these applications will allow you to create own themes based on the bundled ones.
The created themes will be stored in the new JSON format of KSyntaxHighlighting and can be used as is for e.g. [submission to us](/post/2020/2020-07-18-contributing-via-gitlab-merge-requests/).

### How was it before Frameworks 5.75?

Before 5.75, KTextEditor already had support for color themes (or schemes).
But we just shipped a very limited amount (as KConfig based bundled configs).

We only had Breeze & Solarized (as Light and Dark), a Vim Dark and a Printing theme.

There were no real additions of new themes in the past years, thought some are sprinkled around the internet.

### How will 5.75 change that?

The 5.75 KSyntaxHighlighting framework release will at least feature all themes we had before and in addition:

* Dracula
* ayu Dark
* ayu Light
* ayu Mirage
* gruvbox Dark
* gruvbox Light

Beside this, Vim Dark got some overhaul to be more consistent with the real Vim theme it is based on.

This means we have now already doubled the number of themes an user can choose between on a default installation.

I think this is a nice improvement.
Thanks to all people that did help to make this possible!

For preview renderings of all themes we offer, visit our [color themes page](/themes).
This page is auto-generated and will be periodically updated with the current state of the master branch of [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/).

For people just reading this post, here an preview of **ayu Mirage**:

{{< rawhtml >}}
{{% include "/static/themes/html/ayu-mirage-snippet.html" %}}
{{< /rawhtml >}}

### The future => We want more, please help out!

With the recent additions we already cover some more well known text editor color themes.
But if you just search a bit around the internet or look what other text editors ship per default, we still lack a lot of well known ones.

For example even our GitLab instance provides the Monokai theme in the [configuration](https://invent.kde.org/help/user/profile/preferences#syntax-highlighting-theme) for its web highlighting that we still lack.

Therefore, we are eager to get submissions for more MIT licensed color themes we can bundle with KSyntaxHighlighting.

All users of applications using this framework will enjoy to be able to choose between more themes with ease if you help us!

Therefore, take the chance and help us out, provide some more themes as [merge request](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests).

License must be MIT, this seems to be no problem for most themes out there, at least it seems most of the ones I stumbled over are MIT licensed.

If you need to have some template how such a submission shall look, here the requests that got merged for the [ayu themes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/65) and the [gruvbox themes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/66).

We have some [development issue open](https://invent.kde.org/frameworks/syntax-highlighting/-/issues/2) that tracks a bit what happend so far.

But please don't just show there up to say "hey people, I want to have theme X".
This is not helpful ;=)

Just searching for "popular text editor color themes" or something like that already gives a good idea which themes people yearn for.
We don't need some more pointers.

What is helpful is to show up with a MIT licensed theme as [a merge request](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests), as seen above.

Hope to see more themes submitted, scratch your own itch, but please contribute the stuff back to us upstream!
