---
title: Kate in the Windows Store - Current Status
author: Christoph Cullmann
date: 2020-09-28T22:40:00+02:00
url: /post/2020/2020-09-28-kate-windows-store-current-status/
---

## One year in the store

Kate is now in the [Windows Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW) since a bit over one year, our initial submission went online on [September 12, 2019](/post/2019/2019-09-12-kate-in-the-windows-store/).

The submitted installers are always taken 1:1 from our very lovely [Binary Factory](https://binary-factory.kde.org/).
They just get some test drive if they behave well and then are submitted like described [here](/post/2019/2019-11-03-windows-store-submission-guide/) with the [KDE e.V.](https://ev.kde.org) account.

Therefore the most work was done by all the people that created and keep the [Craft](https://community.kde.org/Craft) tooling & [Binary Factory](https://binary-factory.kde.org) working!
Thanks a lot to our awesome system administrators, Hannah and all other involved people ;=)

Hannah reminded me that it is time for an updated version of Kate, too, the last submission was the 20.04 release.
Therefore at the moment the 20.08 release is in the submission queue and should arrive in the store in the next days.
It will featuring all nifty new 20.08 features and some post 20.08 bug fixes.

## Over 60,000 installations, nice!

Since the initial submission to the store, Kate has now been installed 60,346 times.
That means roughly 5,000 new installations per month!

This might pale compared to a lot of other projects out there, but given we are for example a lot longer available on Windows via e.g. [Chocolatey](https://chocolatey.org/packages/kate), but have there only 8,796 downloads since ever, this is really awesome.

Perhaps we are even more often installed via direct downloads from the Binary Factory installers as linked on our [Get Kate](/get-it/) page, but still, it is nice to see that there is actually interest in our software on Windows, and not just by a handful of people.

I hope this interest in our text editor will expand in the future and might result in more contributors on Windows for both Kate and the overall KDE community.

If you are interested in our stuff, please help out to improve it.
For example [code contributions](/post/2020/2020-07-18-contributing-via-gitlab-merge-requests/) are easier than ever since we moved to our new and shiny [KDE GitLab instance](https://invent.kde.org).

## Reviews?

Now, after a lot of self-praise, let's take a look at our reviews in the Windows Store.

The internal store web interface shows that we have overall 96 reviews since we are in the store and that the overall rating is 4.7 stars.
The concrete results breakdown can be seen below.

<p align="center">
    <img src="/post/2020/2020-09-28-kate-windows-store-current-status/images/kate-windows-store-reviews.png">
</p>

For me, this is actually a very nice feedback.
Unlike some comments we got on reddit and Co. after the initial publishing, no, we didn't end up with just one star ratings in no time.

### How users praise us!

Here some examples of reviews that should make our contributors happy to read (without any names & with some grave spelling mistakes that Kate's spell checker underlined for me fixed, and no, I didn't add that heart smiley Unicode char, that was there in that one review, it is a 1:1 copy):

> Wow! Finally. This superb, multifunctional and so very feature rich editor from Linux is now available on Windows! And as an app, so we get automatic updates along with other Windows apps! Sweet💖

> The perfect app to substitute the default notepad of Windows 10.

> A powerful text editor with viewer option and code editor

> I was a user of KDE plasma I thought before downloading this I thought there will be a new interface but the fonts tabs themes were all KDE Plasma THIS IS FAR MORE BETTER THAN NOTEPAD....

> Works flawlessly, easily replaces windows notepad, notepad++. Plus its free AND open source. Comes from KDE so that's another great thing. Really loved the work from KDE into this product. Thanks Developers & contributors

> It's always my first choice for a text editor. I really appreciate KDE's work

> The best editor, on any platform. It's my favorite on any platform. First used it on Linux with KDE3, and been in love with it ever since. Highly recommended!

> This is a very powerful open-source editor (and works great on high-DPI monitors too).

> It feel in love with Kate on Kubuntu and now I use it everywhere! Thanks KDE for making great software

> This is Kate from KDE, now on Windows. HiDPI works (I'd say better than fractional scaling in Kubuntu, even), everything works, and it's a nice editor.

> I love Kate Text Editor, been using it on Windows and Linux for a year or two now. I like having a reliable, simple-like-notepad, yet with multi-tabs, sessions, and plenty of additional features if I want them (coding, etc.), thanks to the team for keeping this excellent application up to date! Glad there's a Windows Store (for better or worse) version to make it easier for Windows users / those confused by the web-site's download setup, to get it, and for it to auto-update also.

### Do areas to improve exist? For sure!

Yet, there is not just praise, naturally.
Here some reviews that point to areas we shall improve:

> The application is very good and complete but the graphical user interface is not really polished for Windows which if it causes me conflict, fix that, keep it up.

> There are some rendering issues. I loved Kate on Linux. Also my system language settings are set to English, yet it displays in a different language.

> I'm used to KDE applications from Linux, and I'm very glad that it is now available on Windows as well. I really like these kind of oldschool editors compared to e.g. Visual Studio Code, because of their simple interface and exactly the right amount of features. The only thing I miss from Linux is the terminal-integration. Would be great to have KDevelop in the store as well!

> It is one of the best note editor that can be found in the Microsoft Store. But I think the size of the app is too big without any reason.

> Amazing app from KDE, but I have to admit it looks much better in Linux

> Very good. However, it does not save the changes we do the editor's fonts and colors, which makes it unusable for me.

> A good editor, I uses it in Linux and now that I have to work in windows, I'm glad to find it here also. I dislike the Microsoft store version because I cannot set it as default editor for .java files making it very tiresome to use. Uninstalling it.

> This is a very clean port to Windows. It maintains its core-feature parity between Linux, Windows and Mac. That's not such a bad thing, if you are looking strictly for a text editor. Where things start to distinguish are the lack of terminal support, and or plugins. When you have options like VS Code (Insiders) which have WSL coding options (remote WSL from Windows), I don't see why I would go with Kate. But, I expect good things to come, as they work to include WSL, and get the feature parity back

> Perfect! Just a small issue: in 4K monitors the icon seems too small (this is not an issue if you install it normally by downloading the installer). Not sure where should I report this bug :\\

> When using a dark theme, the icons keep dark as they are in light theme, there is no light icons to use with a dark theme

> The code highlight feature is great. It would be better if you could have embedded terminals like the Linux version.

> I was waiting for this day. KDE is awesome. I use KDE Neon everyday. KDE has a lots of features that Windows can't offer. Kate is one of my favorite program since I found Kate. I just want a feature on Kate which is, like notepad++ when we close the program it automatically saves all tabs and files. So next time when we start the program. It starts like the last session and automatically loads everything for you. Thanks...

> One star deducted as Dark theme icons are black (should be white or at least not black) otherwise awesome quick editor. Thanks

If somebody has time to work on any of the above issues, please consider to [contribute](/post/2020/2020-07-18-contributing-via-gitlab-merge-requests/) improvements to our stuff.
Some small stuff like improving the shipped icon or enhancing which file extensions we support was already done.
Other stuff like better handling of dark/light mode for icons is still [work in progress](https://phabricator.kde.org/D25119) and any help wanted!

## The Future?

I hope more people will join the effort to bring KDE software to Windows.
As you can see above, it is appreciated by Windows users, if the port is done in a reasonable way.

I think having more of our stuff there (and on any other platform) will help to keep our software relevant by broadening our user & contributor base.

Beside this, it improves the quality of our software, as alone having to compile it with a very different compiler and different underlying libraries often uncovers existing issues.

Many fixes done for Windows will benefit other platforms, too, like e.g. the better deployability of our KDE Frameworks we worked on in the past.
This was driven a lot by the needs of the Windows (and macOS) packaging that is very different to how Linux distributions deploy our stuff.
