---
title: 'Kate & Akademy Awards 2016'
author: Christoph Cullmann

date: 2016-09-07T12:34:23+00:00
url: /2016/09/07/kate-akademy-awards-2016/
categories:
  - Common
  - Developers
  - Events
  - KDE
  - Users
tags:
  - planet

---
Dominik and me got the Akademy 2016 Award for our work on Kate and KTextEditor.

<img class="aligncenter wp-image-3956 size-large" src="/wp-content/uploads/2016/09/29370954391_9422233b80_o-1024x673.jpg" alt="29370954391_9422233b80_o" width="474" height="312" srcset="/wp-content/uploads/2016/09/29370954391_9422233b80_o-1024x673.jpg 1024w, /wp-content/uploads/2016/09/29370954391_9422233b80_o-300x197.jpg 300w, /wp-content/uploads/2016/09/29370954391_9422233b80_o-768x505.jpg 768w" sizes="(max-width: 474px) 100vw, 474px" /> 

I want to pass that on to all other contributors of both the application and the framework: **You all rock and people seem to appreciate our work!**

Lets keep on improving our stuff and providing people with a very usable editor and editor component for various use cases.