---
title: Updated bundles, now release builds and less frameworks
author: Christoph Cullmann

date: 2016-06-13T22:21:36+00:00
url: /2016/06/14/updated-bundles-now-release-builds-and-less-frameworks/
categories:
  - KDE
  - Users
tags:
  - planet

---
I updated the bundles, now they are release builds of Kate & KWrite and they contain less frameworks. The size is only one half of the last bundles, less than 50 MB per app. Visit &#8220;[Get It][1]&#8221; for the download links or below.

### Mac Application Bundles

  * [Kate 16.07.70 (Update 1)][2]
  * [KWrite 16.07.70 (Update 1)][3]

[<img class="aligncenter" src="https://www.kde.org/fundraisers/randameetings2016/images/banner-fundraising2016.png" width="1400" height="200" />][4]

 [1]: /get-it/
 [2]: ftp://babylon2k.de/cullmann/kate-16_07_70_update_1.dmg
 [3]: ftp://babylon2k.de/cullmann/kwrite-16_07_70_update_1.dmg
 [4]: https://www.kde.org/fundraisers/randameetings2016/