---
title: KSyntaxHighlighting – A new Syntax Highlighting Framework
author: Dominik Haumann

date: 2016-11-15T21:35:36+00:00
url: /2016/11/15/ksyntaxhighlighting-a-new-syntax-highlighting-framework/
categories:
  - Developers
  - KDE
tags:
  - planet

---
Today, [KDE Frameworks 5.28][1] was released with the brand new KSyntaxHighlighting framework. The announcement says:

> <p style="padding-left: 30px;">
>   New framework: syntax-highlighting<br /> Syntax highlighting engine for Kate syntax definitions
> </p>
> 
> <p style="padding-left: 30px;">
>   This is a stand-alone implementation of the Kate syntax highlighting engine. It&#8217;s meant as a building block for text editors as well as for simple highlighted text rendering (e.g. as HTML), supporting both integration with a custom editor as well as a ready-to-use QSyntaxHighlighter sub-class.
> </p>

This year, on March 31st, KDE&#8217;s advanced text editor Kate had its [15th birthday.][2] 15 years are a long time in the software world, and during this time Kate won the hearts of many users and developers. As text editing component, Kate uses the KTextEditor framework, which is used also by applications such as [KDevelop][3] or [Kile][4].

The KTextEditor framework essentially is an embeddable text editing component. It ships everything from painting the line numbers, the background color, the text lines with syntax highlighting, the blinking cursor to code completion and [many more features][5]. One major feature is its very powerful syntax highlighting engine, enabling us to properly highlight around 275 languages.

Each syntax highlighting is defined in terms of an xml file ([many examples][6]), as described in [Kate&#8217;s documentation][7]. These xml files are read by KTextEditor and the context based highlighting rules in these files are then used to highlight the file contents.

For the last 15 years, this syntax highlighting engine was tightly coupled with the rest of the KTextEditor code. As such, it was not possible to simply reuse the highlighting engine in other projects without using KTextEditor. This lead to the unfortunate situation, where e.g. the Qt Creator developers partly reimplemented Kate&#8217;s syntax highlighting engine in order to support other languages next to C/C++.

This changed as of today: The [KSyntaxHighlighting framework][8] is a [tier 1 functional framework][9] that solely depends on Qt (no dependency on Qt Widgets or QML), is very well unit tested, and licensed under the LGPLv2+. As mentioned in the announcement and in the [API documentation][10], it is a stand-alone implementation of the Kate syntax highlighting engine. It&#8217;s meant as a building block for text editors as well as for simple highlighted text rendering (e.g. as HTML), supporting both integration with a custom editor as well as a ready-to-use [QSyntaxHighlighter sub-class][11]. This also implies that you can reuse this framework to add syntax highlighting to e.g. QML applications.

We hope that other applications such as Qt Creator will start to use the KSyntaxHighlighting framework, since it allows us to cleanly share one single implementation of the syntax highlighting engine.

In the next KDE Frameworks releases, we will remove KTextEditors syntax highlighting engine in favor of just using KSyntaxHighlighting. This will happen step by step. For instance, we already have [a pending patch][12] that removes all xml files from KTextEditor.git in favor of using the ones shipped by the KSyntaxHighlighting framework. That means, with the KDE Frameworks 5.29 release, Kate&#8217;s and KTextEditors dependency (and other application&#8217;s dependencies) will look as follows:

[<img class="aligncenter size-full wp-image-3983" src="/wp-content/uploads/2016/11/kte-kf5.png" alt="KTextEditor and KSyntaxHighlighting" width="696" height="293" srcset="/wp-content/uploads/2016/11/kte-kf5.png 696w, /wp-content/uploads/2016/11/kte-kf5-300x126.png 300w" sizes="(max-width: 696px) 100vw, 696px" />][13]

This is quite an interesting change, especially since moving the syntax highlighting engine out of KTextEditor was already planned since [Akademy 2013 in Bilbao][14]:

> <p style="padding-left: 30px;">
>   Another idea was raised at this year’s <a title="Akademy 2013" href="http://akademy2013.kde.org/" target="_blank" rel="noopener">Akademy in Bilbao</a>: Split Kate Part’s highlighting into a separate library. This way, other applications could use the Kate Part’s highlighting system. Think of a command line tool to create highlighted html pages, or a syntax highlighter for QTextEdits. The highlighting engine right now is mostly internal to Kate Part, so such a split could happen also later after the initial release of KTextEditor on 5.
> </p>

This goal is now reached &#8211; thanks to Volker Krause who did most of the work. Pretty cool!

If you are interested in using the KSyntaxHighlighting framework, feel free to [contact us on our mailing list][15]. Further, we welcome all contributions, so please send patches to our mailing list, or post them on [phabricator][16]. (You can also find the [KSyntaxHighlighting framework on github][17] for convenience, but it&#8217;s not our primary platform).

You can also support Kate and the KDE Frameworks by [donating to the KDE e.V.][18], KDE&#8217;s non-profit organization.

**<span style="color: #ff0000;">Update:</span> Changed XML Syntax Definition File Location**

Starting with KDE Frameworks 5.29, the KTextEditor framework now uses the syntax highlighting files from KSyntaxHighlighting. These files are located in <tt>$HOME/.local/share/org.kde.syntax-highlighting/syntax</tt>. If this folder does not exist, just create it. Futher, you will not find any syntax highlighting files in your system installation, since the syntax highlighting files shipped with KSyntaxHighlighting are compiled into the executable.

 [1]: https://www.kde.org/announcements/kde-frameworks-5.28.0.php
 [2]: /2010/08/15/kate-history/
 [3]: https://www.kdevelop.org/
 [4]: https://userbase.kde.org/Kile
 [5]: /about-kate/
 [6]: /syntax/5.28/
 [7]: https://docs.kde.org/stable5/en/applications/katepart/highlight.html
 [8]: https://phabricator.kde.org/diffusion/216/repository/master/
 [9]: https://dot.kde.org/2013/09/25/frameworks-5
 [10]: https://api.kde.org/frameworks/syntax-highlighting/html/index.html
 [11]: https://api.kde.org/frameworks/syntax-highlighting/html/classKSyntaxHighlighting_1_1SyntaxHighlighter.html
 [12]: https://git.reviewboard.kde.org/r/129384/
 [13]: /wp-content/uploads/2016/11/kte-kf5.png
 [14]: /2013/11/11/kate-on-5-the-future-of-ktexteditor-and-kate-part/
 [15]: mailto:kwrite-devel@kde.org
 [16]: https://phabricator.kde.org/differential/diff/create/
 [17]: https://github.com/KDE/syntax-highlighting
 [18]: https://www.kde.org/community/donations/index.php#money