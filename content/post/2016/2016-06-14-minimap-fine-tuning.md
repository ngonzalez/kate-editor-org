---
title: MiniMap Fine-Tuning
author: Dominik Haumann

date: 2016-06-14T18:06:56+00:00
url: /2016/06/14/minimap-fine-tuning/
categories:
  - Events
  - Users
tags:
  - planet
  - sprint

---
Kate also supports a minimap of the text contents instead of a classical scrollbar. To enable it, just go to

<img class="aligncenter size-full wp-image-3837" src="/wp-content/uploads/2016/06/minimap-settings.png" alt="MiniMap Settings" width="911" height="739" srcset="/wp-content/uploads/2016/06/minimap-settings.png 911w, /wp-content/uploads/2016/06/minimap-settings-300x243.png 300w, /wp-content/uploads/2016/06/minimap-settings-768x623.png 768w" sizes="(max-width: 911px) 100vw, 911px" /> 

Here at the Randa meeting, we just invested some time to bring this minimap more in line with the Breeze style. You can see the difference here: Note the scrollbar slider frame, the new version (right) is more distinct and in line with the rounded corners of the Breeze style.

<table style="width: 100%;">
  <tr>
    <td>
      <img class="aligncenter size-full wp-image-3838" src="/wp-content/uploads/2016/06/minimap-old.png" alt="MiniMap - Old Slider" width="471" height="375" srcset="/wp-content/uploads/2016/06/minimap-old.png 471w, /wp-content/uploads/2016/06/minimap-old-300x239.png 300w" sizes="(max-width: 471px) 100vw, 471px" /></p> 
      
      <p>
        Old Scrollbar Slider</td> 
        
        <td>
          <img class="aligncenter size-full wp-image-3839" src="/wp-content/uploads/2016/06/minimap-new.png" alt="MiniMap - New Slider" width="469" height="369" srcset="/wp-content/uploads/2016/06/minimap-new.png 469w, /wp-content/uploads/2016/06/minimap-new-300x236.png 300w" sizes="(max-width: 469px) 100vw, 469px" /></p> 
          
          <p>
            New Scrollbar Slider</td> </tr> </tbody> </table> 
            
            <p>
              This change will be in the KDE Frameworks 5.24 release. If you want to support the KDE Randa developer meeting, please click on the banner :-)<br /> <a href="https://www.kde.org/fundraisers/randameetings2016/"><img class="aligncenter" src="https://www.kde.org/fundraisers/randameetings2016/images/banner-fundraising2016.png" width="1400" height="200" /></a>
            </p>