---
title: New Kate/KWrite Bundles for Mac
author: Christoph Cullmann

date: 2016-06-16T10:57:09+00:00
url: /2016/06/16/new-katekwrite-bundles-for-mac/
categories:
  - Common
  - Developers
  - Events
  - KDE
  - Users
tags:
  - planet

---
I updated again the Kate/KWrite application bundles following the new guide on [KDE on Mac][1].

With the same guide, other application developers should be able to create stand-alone application bundles for Mac OS X, too. Btw., the guide is on a wiki page, feel free to enhance the documentation, I doubt it is optimal ATM.

What needs to be solved to make it easier: At the moment, the guide uses a stock Qt as obtainable from qt.io but unfortunately, macdeployqt needs to be patched to deploy non-qt plugins

For this see the review request [&#8220;add support for -extra-plugins command line option&#8221;][2]. I hope this gets into Qt or we get an alternative solution, hint, hint => help appreciated :=))

KWrite on Mac looks unspectacular:

<img class="aligncenter wp-image-3875" src="/wp-content/uploads/2016/06/Bildschirmfoto-2016-06-16-um-12.24.46-1024x836.png" alt="Bildschirmfoto 2016-06-16 um 12.24.46" width="600" height="490" srcset="/wp-content/uploads/2016/06/Bildschirmfoto-2016-06-16-um-12.24.46-1024x836.png 1024w, /wp-content/uploads/2016/06/Bildschirmfoto-2016-06-16-um-12.24.46-300x245.png 300w, /wp-content/uploads/2016/06/Bildschirmfoto-2016-06-16-um-12.24.46-768x627.png 768w, /wp-content/uploads/2016/06/Bildschirmfoto-2016-06-16-um-12.24.46.png 1306w" sizes="(max-width: 600px) 100vw, 600px" /> 

But the cool thing is: This is the result of a plain compile of KF5 & kate.git. We patch NONE of the frameworks anymore and we don&#8217;t need to build Qt on our own, we can use the stock Qt installer (the bundles use 5.7.0, the build that should be the final one to be released these days).

For deployment problems like: [&#8220;how to get our icons&#8221;][3] or &#8220;how to locate our plugins or ioslaves&#8221; generic solutions were implemented in the KF5 libraries.

If the above review request is accepted or an other solution is found you will have exactly the same developer experience on Mac as on e.g. Linux. No ugly hacks, no patching, etc.

For Kate we even have a kind of working embedded Konsole again with this updated build, enjoy:

<img class="aligncenter wp-image-3874" src="/wp-content/uploads/2016/06/Bildschirmfoto-2016-06-16-um-12.22.34-1024x800.png" alt="Bildschirmfoto 2016-06-16 um 12.22.34" width="600" height="469" srcset="/wp-content/uploads/2016/06/Bildschirmfoto-2016-06-16-um-12.22.34-1024x800.png 1024w, /wp-content/uploads/2016/06/Bildschirmfoto-2016-06-16-um-12.22.34-300x234.png 300w, /wp-content/uploads/2016/06/Bildschirmfoto-2016-06-16-um-12.22.34-768x600.png 768w" sizes="(max-width: 600px) 100vw, 600px" /> 

I think overall, the Mac developer experience for KF5 based stuff really improved in the last months and the latest things that happened this week were only possible thanks to the [Randa sprint][4]. Would be nice if you [support us][5] ;)  
[<img class="aligncenter" src="https://www.kde.org/fundraisers/randameetings2016/images/banner-fundraising2016.png" width="1400" height="200" />][5]

 [1]: https://community.kde.org/Mac#Building_KDE_Frameworks_based_software_on_Mac_OS_X
 [2]: https://codereview.qt-project.org/#/c/162160/
 [3]: https://blogs.kde.org/2016/06/16/icon-theme-deployment-windows-mac-os-and-mobile-platforms
 [4]: http://randa-meetings.ch/
 [5]: https://www.kde.org/fundraisers/randameetings2016/