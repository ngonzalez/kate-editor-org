---
title: 'Kate XML Completion Plugin: Help Wanted'
author: Dominik Haumann

date: 2010-01-17T12:58:00+00:00
url: /2010/01/17/kate-xml-completion-plugin-help-wanted/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2010/01/kate-xml-completion-plugin-help-wanted.html
categories:
  - Developers

---
In KDE3 Kate had a plugin called &#8220;XML Completion Plugin&#8221;. It was able to list XML elements, attributes and attribute values and entities in a completion popup menu depending on the currently active DTD. For instance, it was able to complete all your KDE/docbook tags and, thus, was one of the best tools for writing KDE documentation.

Unfortunately, [this plugin has not been ported to KDE4][1], yet. So this is a request for developers: Please someone pick it up and make it available for Kate in KDE4.

The location in svn is: [trunk/KDE/kdesdk/kate/plugins/xmltools][2]  
Just enable this folder in the kate/plugins/CMakeLists.txt file and start porting it. The code completion interface changed quite a lot, but there are other plugins and good api documentation ([KTextEditor interfaces][3], [Kate Application interfaces][4]) where you can have a look at how things work.

Having a working XML Completion plugin for KDE4.5 would be awesome.

 [1]: https://bugs.kde.org/show_bug.cgi?id=222859
 [2]: http://websvn.kde.org/trunk/KDE/kdesdk/kate/plugins/xmltools
 [3]: http://api.kde.org/4.x-api/kdelibs-apidocs/interfaces/ktexteditor/html/index.html
 [4]: http://api.kde.org/4.x-api/kdesdk-apidocs/kate/interfaces/kate/html/index.html