---
title: Featured Articles
author: Dominik Haumann

date: 2010-08-12T12:38:11+00:00
url: /2010/08/12/featured-articles/
categories:
  - Developers
  - Users
tags:
  - planet

---
Looking back at the last month, migrating our <a title="Kate Homepage" href="http://www.kate-editor.org" target="_self">Kate homepage</a> over to WordPress was a vast success.﻿ The useful content we had on our old Drupal was copied over. The benefit of WordPress is that we now have a nice blog software as well with which we even aggregate some external blogs related to Kate. The new homepage is also more structured by having a list of <a title="Featured Articles" href="/featured-articles/" target="_self">featured articles</a> showing useful resources, such as links to user or developer documentation.

The idea behind of the featured articles is to provide links to blog entries explaining in more depth what you can do with Kate. There are lots of useful tricks probably even some KDE developers aren&#8217;t aware of. If you have further hints of how to efficiently use Kate, please <a title="Contact Us" href="/support/" target="_self">contact us</a> and write a blog entry on kate-editor.org.

Besides that, thanks to all contributors for our <a title="Kate in KDE 4.5" href="/2010/08/06/kde-4-5-is-approaching-thanks-to-all-kate-contributors/" target="_self">awesome release in 4.5</a>! :)