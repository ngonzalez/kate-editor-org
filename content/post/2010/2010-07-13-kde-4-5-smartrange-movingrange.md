---
title: 'KDE 4.5: SmartRange => MovingRange'
author: Christoph Cullmann

date: 2010-07-13T13:57:46+00:00
url: /2010/07/13/kde-4-5-smartrange-movingrange/
categories:
  - Developers
tags:
  - planet

---
Dominik already [blogged][1] about the issues we have in KatePart with the current SmartRange/SmartCursor/Smart* interfaces that were designed during the early KDE 4.0 release.

Given that large amounts of the internal implementation are exposed to the outside in the interfaces, there was no feasible way to fix the issues with them. The problem with the not thread-safe behaviour could have been prevented by just stating the plain fact that multi-threaded access to the ranges doesn&#8217;t work, even thought there is a mutex in the interface exposed which should be locked to gain this safety. Still the real problems of the unchangable bad implemenation and design choices would have remained.

Given the fact, that the SmartInterface is an additional interface for a KTextEditor component and no component is required to implement it, we went the way to create a new redesigned additional interface, the MovingInterface.

In short: it is a stripped down version of the SmartInterface, which tries to avoid to expose any implementation details and is focused on provide the features actually used by other applications like KDevelop and have them working correct instead of providing just a mass of features, which doesn&#8217;t work at all in all corner cases.

For KDE 4.5, to not just break all applications which rely on the SmartInterface be implemented in KatePart, both interfaces will be implemented there.

As the SmartRange stuff somehow slipped into the CodeCompletionModelControllerInterface, we will provide a new version of this interface, too, version 3, subsuming 1 + 2, but without SmartRanges inside.

For KDE 4.6, KatePart won&#8217;t implement any longer the SmartInterface nor the old CodeCompletionModelControllerInterface(2), but:

  * MovingInterface
  * CodeCompletionModelControllerInterface3

If your application relies on any of these two interfaces to be implemented (or will be crippled without them), you should switch over to the new interfaces and require KDE 4.5, as soon as it is released.  
That way the applications will just keep working with KDE 4.5 and higher.

KDevelop already is on the way to do so, hope others might follow (the interfaces are not widely used it seems, that at least means not that much applications are hit, KDevelop and Kile are for sure the major ones).

I can understand that this change might not be really liked by any project using the interfaces, but the plain facts are:

  * the code doesn&#8217;t work
  * it is not maintained
  * nobody still around really understands it
  * the BC of the interfaces disallows to fix the design flaws without a new interface

There will be more information for about the new stuff in the future,  but thought it is a good idea to remember people outside of the Kate development about the changes, before KDE 4.5 releases (which still will provide a KatePart implementing the old interfaces).

 [1]: /2010/04/28/kate-internals-smart-cursors-and-smart-ranges/