---
title: KDE Applications Versioning
author: Christoph Cullmann

date: 2015-07-26T12:25:23+00:00
url: /2015/07/26/kde-applications-versioning/
categories:
  - Common
  - Developers
  - KDE
tags:
  - planet

---
A common problem for many applications contained in the KDE Applications releases are non-incremented version numbers. Often the maintainer forgets to update the version number of the application, like I did it for Kate since the first KF5 based release.

This means: On Bugzilla, I get bugreports that always tell me &#8220;Kate 5.0.0&#8221;, not very helpful.

KDE Frameworks solves this by automatic setting of the current framework release version in all framework CMakeLists.txt.

For KDE Applications, we have now optional the same concept. For details: visit <a href="https://community.kde.org/Applications/Versioning" target="_blank">https://community.kde.org/Applications/Versioning</a>

In short: If you are to lazy to update your version number yourself or are just fine with using the same version number als the KDE Applications releases, you can add the following three lines to your toplevel CMakeLists.txt:

`# KDE Application Version, managed by release script<br />
set (KDE_APPLICATIONS_VERSION_MAJOR "15")<br />
set (KDE_APPLICATIONS_VERSION_MINOR "04")<br />
set (KDE_APPLICATIONS_VERSION_MICRO "0")`

These variables will then be patched by the release scripts to the &#8220;right&#8221; version for the current release series.  
You can than either just use the MICRO version to suffix your own applications version to differentiate the bugfix releases or like Kate construct your complete version number from the three vars and be done ;=)

I hope this helps to have more consistent and more important meaningful version numbers once again in the applications we bundle. Thanks to all people that made this &#8220;magic&#8221; happen ;=)