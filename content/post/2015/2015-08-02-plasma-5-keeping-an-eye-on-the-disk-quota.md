---
title: 'Plasma 5: Keeping an Eye on the Disk Quota'
author: Dominik Haumann

date: 2015-08-02T09:39:43+00:00
url: /2015/08/02/plasma-5-keeping-an-eye-on-the-disk-quota/
categories:
  - Common
  - Developers
  - Users
tags:
  - planet

---
At this year&#8217;s KDE conference Akademy, I was working on a small plasmoid to continuously track the disk quota.

The disk quota is usually used in enterprise installations where network shares are mounted locally. Typically, sysadmins want to avoid that users copy lots of data into their folders, and therefor set quotas (the quota limit has nothing to do with the physical size of a partition). Typically, once a user gets over the hard limit of the quota, the account is blocked and the user cannot login anymore. This happens from time to time, since the users are not really aware of the current quota limit and the already used disk space.

### Disk Quota as Plasmoid

Here is where the &#8220;Disk Quota&#8221; plasmoid helps: Once added to the panel, it shows all quota limits when clicking on the plasmoid:

[<img class="aligncenter size-full wp-image-3603" src="/wp-content/uploads/2015/08/diskquota.png" alt="Disk Quota" width="537" height="382" srcset="/wp-content/uploads/2015/08/diskquota.png 537w, /wp-content/uploads/2015/08/diskquota-300x213.png 300w" sizes="(max-width: 537px) 100vw, 537px" />][1]On mouse over, a nice tool tip shows the used quota in percent (in case multiple quota limits exist, the highest quota is displayed):

[<img class="aligncenter size-full wp-image-3604" src="/wp-content/uploads/2015/08/diskquota-tooltip.png" alt="Disk Quota ToolTip" width="498" height="188" srcset="/wp-content/uploads/2015/08/diskquota-tooltip.png 498w, /wp-content/uploads/2015/08/diskquota-tooltip-300x113.png 300w" sizes="(max-width: 498px) 100vw, 498px" />][2]When a quota is > 50% but smaller than 75%, the quota icon gets orange:

[<img class="aligncenter size-full wp-image-3605" src="/wp-content/uploads/2015/08/diskquota-low.png" alt="Disk Quota > 50%" width="543" height="379" srcset="/wp-content/uploads/2015/08/diskquota-low.png 543w, /wp-content/uploads/2015/08/diskquota-low-300x209.png 300w" sizes="(max-width: 543px) 100vw, 543px" />][3] And if the quota is between 75% and 90%, the quota icon gets red: [<img class="aligncenter size-full wp-image-3607" src="/wp-content/uploads/2015/08/diskquota-medium1.png" alt="diskquota-medium" width="542" height="366" srcset="/wp-content/uploads/2015/08/diskquota-medium1.png 542w, /wp-content/uploads/2015/08/diskquota-medium1-300x203.png 300w" sizes="(max-width: 542px) 100vw, 542px" />][4]Finally, a quota > 90% will urge the user to cleanup data by showing a exclamation mark as well:

[<img class="aligncenter size-full wp-image-3608" src="/wp-content/uploads/2015/08/diskquota-high.png" alt="High Disk Quota" width="535" height="376" srcset="/wp-content/uploads/2015/08/diskquota-high.png 535w, /wp-content/uploads/2015/08/diskquota-high-300x211.png 300w" sizes="(max-width: 535px) 100vw, 535px" />][5]Clicking on a quota entry will launch <a href="https://en.wikipedia.org/wiki/Filelight" target="_blank">Filelight</a> in the correct folder, if it is installed.

### Disk Quota in System Tray

If the quota plasmoid is shown in the system tray (configure the systray and add it there), then the quota information is shown inline in the system tray popup:

[<img class="aligncenter size-full wp-image-3609" src="/wp-content/uploads/2015/08/diskquota-systray.png" alt="Disk Quota in System Tray" width="654" height="510" srcset="/wp-content/uploads/2015/08/diskquota-systray.png 654w, /wp-content/uploads/2015/08/diskquota-systray-300x234.png 300w" sizes="(max-width: 654px) 100vw, 654px" />][6]As soon as the quota is equal to or greater than 50%, the quota icon automatically gets visible in the system tray panel:

[<img class="aligncenter size-full wp-image-3610" src="/wp-content/uploads/2015/08/diskquota-systray-medium.png" alt="Disk Quota - Attention" width="691" height="507" srcset="/wp-content/uploads/2015/08/diskquota-systray-medium.png 691w, /wp-content/uploads/2015/08/diskquota-systray-medium-300x220.png 300w" sizes="(max-width: 691px) 100vw, 691px" />][7]Finally, if the quota is >= 98%, the quota icon pulses to get the user&#8217;s attention (a nice Plasma 5 feature!).

To decrease the quota again, simply click on the respective quota entry in the list view. This launches <a href="https://en.wikipedia.org/wiki/Filelight" target="_blank">Filelight</a> with the correct location.

### Code Internals

As of now, the Disk Quota plasmoid internally calls the command line tool &#8216;**quota**&#8216; to retrieve the quota information. The output of this tool is parsed and then interpreted to show the quota information as can be seen in the screen shots. If the quota command line tool is not installed, then the Disk Quota plasmoid shows diagnostics like this:

### [<img class="aligncenter size-full wp-image-3612" src="/wp-content/uploads/2015/08/diskquota-missing.png" alt="Disk Quota missing" width="546" height="367" srcset="/wp-content/uploads/2015/08/diskquota-missing.png 546w, /wp-content/uploads/2015/08/diskquota-missing-300x202.png 300w" sizes="(max-width: 546px) 100vw, 546px" />][8]Infos for Packagers

Since Disk Quota uses the command line tool &#8216;quota&#8217; and launches &#8216;filelight&#8217; when clicking on quota entries, you should ship &#8216;quota&#8217; and &#8216;filelight&#8217; when installing this applet. Most certainly, this plasmoid is only useful for networked systems (with remote mount points) and enterprise installations.

### Release Info

Status: Included in Plasma 5.5. ( <a href="https://git.reviewboard.kde.org/r/124589/" target="_blank">Review Request: Disk Quota</a>)

### Credit where Credit is Due

During implementation lots of questions popped up. Thankfully, we were all at the KDE conference Akademy, and Kai, Sebas, Marco, Martin and the Visual Design group (for design help and icons) were there for quick help &#8211; Thanks a lot! Hope you all like the result :-)

 [1]: /wp-content/uploads/2015/08/diskquota.png
 [2]: /wp-content/uploads/2015/08/diskquota-tooltip.png
 [3]: /wp-content/uploads/2015/08/diskquota-low.png
 [4]: /wp-content/uploads/2015/08/diskquota-medium1.png
 [5]: /wp-content/uploads/2015/08/diskquota-high.png
 [6]: /wp-content/uploads/2015/08/diskquota-systray.png
 [7]: /wp-content/uploads/2015/08/diskquota-systray-medium.png
 [8]: /wp-content/uploads/2015/08/diskquota-missing.png