---
title: Again about Relicensing KDE’s Source Code
author: Dominik Haumann

date: 2015-08-09T19:37:58+00:00
url: /2015/08/09/again-about-relicensing/
categories:
  - Common
  - Developers
  - Users
tags:
  - planet

---
To get started with KDE development, newcomers usually first sent patches. Having sent several patches, the newcomers are typically encouraged by us (the reviewers) to apply for a KDE contributor account. This application includes the question of &#8220;who encouraged you to apply for a KDE contributor account&#8221;.

Then, the KDE sysadmins contact the mentioned developers to make sure the application is valid and trustworthy.  Hence, you, as someone who encouraged others to apply for an account, get a mail with the subject &#8220;Developer account application approval [&#8230;]&#8221;.

To all KDE contributors: Given you probably also were involved with getting other contributors involved with KDE, please follow these steps:

  1. Filter your KDE mails for &#8220;Developer account application approval&#8221;
  2. For each contributor, get the contributors commit name (you can look this up on identity.kde.org > Prople.
  3. Check, whether the new contributor added him/herself to the <a href="https://phabricator.kde.org/source/kde-dev-scripts/browse/master/relicensecheck.pl" target="_blank">relicensecheck.pl</a> script.
  4. If not, please send this contributor a mail and encourage him/her to add him/herself to this script.

You really should check this, since you are also responsible for getting these new contributors commit access.

I did that with 8 contributors. 7 already added themselves within one week. Only one is not reachable by mail anymore&#8230; So it seems to work quite well.