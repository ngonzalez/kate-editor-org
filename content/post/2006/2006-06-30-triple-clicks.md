---
title: Triple clicks
author: Dominik Haumann

date: 2006-06-30T10:13:00+00:00
url: /2006/06/30/triple-clicks/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2006/06/triple-clicks.html
categories:
  - KDE

---
Text editors and line edits support the so-called triple clicks according to [[1]][1]. The document says

  * Triple Click: Select the targeted row. [&#8230;]

It is unclear whether the &#8220;targeted row&#8221; includes the trailing linebreak. Kate Part selects the targeted line including the linebreak. You get the following behavior:

  * If you move the selected row with the mouse, you usually have the linebreaks right. The same applies for copy/cut & paste. If you are used to it, it really is a nice feature.

While this behavior is pretty straightforward, it is not widely in use. If you look at text edit widgets like in firefox, konqueror, Qt, KWord or OOo you will notice that they do not include the trailing newline character.

Question now is: Should we change it for KDE 4 in Kate Part just to be compliant with the others? As Kate Part is an editor component mainly used for programming, my favourite option is to include it, i.e. to keep the current behavior.

(update) I just stumbled over <http://bugs.kde.org/show_bug.cgi?id=91041> =)

[1] <http://developer.kde.org/documentation/standards/kde/style/mouse/selection.html>

 [1]: http://developer.kde.org/documentation/standards/kde/style/mouse/selection.html