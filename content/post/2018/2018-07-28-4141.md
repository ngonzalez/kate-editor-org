---
title: Going to Akademy
author: Dominik Haumann

date: 2018-07-28T10:33:45+00:00
url: /2018/07/28/4141/
categories:
  - Events

---
[<img class="aligncenter size-full wp-image-4142" src="/wp-content/uploads/2018/07/going_to_akademy_banner.jpg" alt="" width="840" height="206" srcset="/wp-content/uploads/2018/07/going_to_akademy_banner.jpg 840w, /wp-content/uploads/2018/07/going_to_akademy_banner-300x74.jpg 300w, /wp-content/uploads/2018/07/going_to_akademy_banner-768x188.jpg 768w" sizes="(max-width: 840px) 100vw, 840px" />][1]Of course, I am going to Akademy. I&#8217;ll be there from Friday 10th to Saturday 18th. See you! :-)

 [1]: https://akademy.kde.org/2018