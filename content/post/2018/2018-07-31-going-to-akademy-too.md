---
title: Going to Akademy, too ;)
author: Christoph Cullmann

date: 2018-07-31T19:43:11+00:00
url: /2018/07/31/going-to-akademy-too/
categories:
  - Events

---
Like Dominik, I will be around the whole Akademy, too.

Looking forward to see new and old friends, lets have a productive week there!

[<img class="aligncenter wp-image-4142 size-full" src="/wp-content/uploads/2018/07/going_to_akademy_banner.jpg" alt="" width="840" height="206" srcset="/wp-content/uploads/2018/07/going_to_akademy_banner.jpg 840w, /wp-content/uploads/2018/07/going_to_akademy_banner-300x74.jpg 300w, /wp-content/uploads/2018/07/going_to_akademy_banner-768x188.jpg 768w" sizes="(max-width: 840px) 100vw, 840px" />][1]

 [1]: https://akademy.kde.org/2018