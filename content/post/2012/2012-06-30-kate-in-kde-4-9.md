---
title: Kate in KDE 4.9
author: Dominik Haumann

date: 2012-06-30T15:16:19+00:00
url: /2012/06/30/kate-in-kde-4-9/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
  - Users
tags:
  - planet

---
With KDE SC 4.9 around the corner and <a title="Kate in KDE 4.7" href="/2011/07/09/kate-in-kde-4-7/" target="_blank">according</a> to <a title="Kate in KDE 4.8" href="/2011/12/21/kate-in-kde-4-8/" target="_blank">tradition</a>, it&#8217;s time to have a look at what&#8217;s new in Kate.

**Improved Support for Custom Color Schemas  
** 

  * improved &#8220;Colors&#8221; tab in the &#8220;Fonts & Colors&#8221; config page
  * configurable colors: search & replace, code folding, indentation line,
  * schema export and import honor background colors (<a title="Background colors are not exported/imported correctly" href="https://bugs.kde.org/show_bug.cgi?id=291868" target="_blank">bug #291868</a>) and font
  * &#8220;highlight selection&#8221; plugin and &#8220;search and replace&#8221; plugin now use search & replace colors from Kate&#8217;s color schema

**Line Modification System**

  * new option to disable in the editor settings: Appearance > Borders > [ ] Show line modification markers
  * colors of the line modification system are configurable

**Build Plugin**

  * reworked config page (including advanced settings for remote debugging)
  * support for remote debugging

**Further Changes**

  * new option in Open/Save config page: [ ] Add newline at end of file on save
  * <a title="Solved issues in Kate for KDE 4.9" href="https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&product=kate&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=RESOLVED&resolution=FIXED&resolution=WORKSFORME&emailassigned_to1=1&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=2012-01-01&chfieldto=2012-06-30&chfieldvalue=&cmdtype=doit&order=Reuse+same+sort+as+last+time&field0-0-0=noop&type0-0-0=noop&value0-0-0=" target="_blank">all fixes</a> in the bug tracker