---
title: Kate/KDevelop Sprint in Vienna
author: Christoph Cullmann

date: 2012-10-21T14:54:18+00:00
url: /2012/10/21/katekdevelop-sprint-in-vienna/
pw_single_layout:
  - "1"
categories:
  - Common
  - Events
  - KDE
tags:
  - planet

---
In some days our nice sprint will start, more information can be found on <a title="Kate/KDevelop Sprint" href="https://sprints.kde.org/sprint/92" target="_blank">KDE Sprints</a>.

I am very happy that <a title="Joseph Wenninger" href="http://www.jowenn.net" target="_blank">Joseph Wenninger</a> organizes this event and the <a title="KDE e.V." href="http://ev.kde.org" target="_blank">KDE e.V.</a> aided with additional sponsorship! Sprints are a real important thing for all KDE projects and it is nice to see that thanks to the help of the community our e.V. is able to sponsor a lot of such events.

I will head over to Vienna on Tuesday, lets see what happens during the week ;)