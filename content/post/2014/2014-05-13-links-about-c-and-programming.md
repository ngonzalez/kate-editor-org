---
title: Links about C++ and Programming
author: Dominik Haumann

date: 2014-05-13T13:28:42+00:00
url: /2014/05/13/links-about-c-and-programming/
categories:
  - Common

---
Just like [some][1] time [ago][2], here are several links that might be of interest:

  * <a title="C++11 compiler support overview" href="http://www.italiancpp.org/wp-content/uploads/2014/03/CppISO-Feb2014-r1.pdf" target="_blank">C++11 compiler support overview</a>
  * <a title="Five new algorithms to C++11" href="http://codexpert.ro/blog/2014/05/07/five-new-algorithms-to-cpp11-you-should-know-about/" target="_blank">Five new algorithms to C++11 that you should know about</a>:  Interesting read about std::all\_of, any\_of, none\_of, is\_sorted, is\_sorted\_until, is_partitioned and some others. (And <a title="Generic numeric algorithms" href="http://codexpert.ro/blog/2014/04/18/generic-numeric-algorithms-in-header-numeric/" target="_blank">here</a> some more generic numeric algorithms)
  * <a title="noexcept - what for?" href="http://akrzemi1.wordpress.com/2014/04/24/noexcept-what-for/" target="_blank">noexcept &#8211; what for?</a> An interesting read about C++11&#8217;s noexcept. Also interesting: <a title="Use noexcept whenever possible" href="http://scottmeyers.blogspot.de/2014/03/declare-functions-noexcept-whenever.html" target="_blank">Scott Meyers&#8217; thoughts</a>.
  * <a title="An overview of C++14 language features" href="http://cpprocks.com/an-overview-of-c14-language-features/" target="_blank">An overview of C++14 language features</a>: Mostly highlights constexpr and auto
  * <a title="C++11 move semantics" href="http://accu.org/content/conf2014/Howard_Hinnant_Accu_2014.pdf" target="_blank">All about C++11 move semantics</a>
  * <a title="Number and string conversion in C++11" href="http://codexpert.ro/blog/2014/04/14/standard-way-of-converting-between-numbers-and-strings-in-cpp11/" target="_blank">Number and string conversion in C++11</a>
  * <a title="What you should know about C++11" href="http://bfilipek.hubpages.com/hub/What-you-should-know-about-C11" target="_blank">What you should know about C++11</a>
  * Lock-free data structures (<a title="Lock-free data structures part 1" href="http://kukuruku.co/hub/cpp/lock-free-data-structures-introduction" target="_blank">part 1</a>, <a title="Lock-free data structures part 2" href="http://kukuruku.co/hub/cpp/lock-free-data-structures-basics-atomicity-and-atomic-primitives" target="_blank">part 2</a>)
  * <a title="Use of assertions" href="http://blog.regehr.org/archives/1091" target="_blank">Use of assertions</a>
  * <a title="Static code analysis of Qt5" href="http://www.viva64.com/en/b/0251/" target="_blank">Static code analysis of Qt5</a>
  * <a title="Stack-based bytecode interpreter" href="http://gameprogrammingpatterns.com/bytecode.html" target="_blank">Interpreter pattern vs. stack-based bytecode interpreter</a>: very good introduction to bytecode interpreters
  * <a title="Overview of design patterns" href="http://www.celinio.net/techblog/wp-content/uploads/2009/09/designpatterns1.jpg" target="_blank">Overview of design patterns</a>
  * <a title="Programming sucks" href="http://stilldrinking.org/programming-sucks" target="_blank">Programming sucks</a>: There is quite some truth in this, although this is probably less valid in open source projects.

 [1]: /2011/08/28/coding-style-and-api-design/ "Coding Style and API Design"
 [2]: /2014/01/17/interfacing-lua-with-templates-in-c11/ "Interfacing lua with C++11"