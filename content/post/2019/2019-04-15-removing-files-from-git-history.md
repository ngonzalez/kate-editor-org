---
title: Removing Files from Git History
author: Christoph Cullmann

date: 2019-04-15T18:00:00+00:00
excerpt: |
  Today I did run again into an old problem:
  You need to archive a lot small and large files inside a single Git repository and you have no support for Git LFS available.
  You did this several year and now you ended up in a state where cloning and working...
url: /posts/removing-files-from-git-history/
enclosure:
  - |
    
    
    
syndication_source:
  - Posts on cullmann.io
syndication_source_uri:
  - https://cullmann.io/posts/
syndication_source_id:
  - https://cullmann.io/posts/index.xml
syndication_feed:
  - https://cullmann.io/posts/index.xml
syndication_feed_id:
  - "11"
syndication_permalink:
  - https://cullmann.io/posts/removing-files-from-git-history/
syndication_item_hash:
  - 36a8eee5bde4016aef8af72c3dd6877f
  - 15418eb559babbd75779289d723b62a8
  - 4905d5c81b9fa24767177fda488f19f0
categories:
  - Common

---
Today I did run again into an old problem: You need to archive a lot small and large files inside a single Git repository and you have no support for [Git LFS][1] available. You did this several year and now you ended up in a state where cloning and working with the repository is unbearable slow.

What now? Last time I did run into that, I archived the overfull repository to some &ldquo;rest in peace&rdquo; space and used `git filter-branch` to filter out no longer needed and too large objects from a repository copy that then will replace the old one for daily use.

There are a lot of guides available how to use `git filter-branch` for that. All variants I ever used were complex to do and did take very long. Especially if you need several tries to get a sane set of stuff you want to remove to gain enough space savings.

This time, I searched once more and stumbled on the [BFG Repo-Cleaner][2]. And yes, it does what it promises on the web site and it seems to be trusted enough to be advertised by e.g. [GitHub][3], too.

Just following the steps described on their [landing page][2] allows to shrink your stuff nicely and without a lot of round-trip time.

If you still are just in the &ldquo;experimenting&rdquo; phase to see which space decrease one can archive with which file size filter (or which files you want to purge by removing them from master before running the tool), I recommend to swap the step

> git reflog expire &ndash;expire=now &ndash;all && git gc &ndash;prune=now &ndash;aggressive

with just

> git reflog expire &ndash;expire=now &ndash;all && git gc &ndash;prune=now

to not wait potential hours for the aggressive GC. For me that was good enough to get some estimate of the later size for my experiments before I settled to some final settings and did the real run.

And as always, if you touch your Git history: **Do that only if you really need to, keep backups, check carefully that afterwards the repository is in some sensible state (`git fsck --strict` is your friend) and inform all people using that repository that they will need to do a full new clone.**

 [1]: https://git-lfs.github.com/
 [2]: https://rtyley.github.io/bfg-repo-cleaner/
 [3]: https://help.github.com/en/articles/removing-sensitive-data-from-a-repository