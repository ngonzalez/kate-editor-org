---
title: External Tools Plugin is Back
author: Dominik Haumann
date: 2019-09-21T21:50:00+02:00
url: /post/2019/2019-09-21-external-tools-plugin/
---

[Back in 2011](https://kate-editor.org/2011/08/09/maintainer-needed-kate-external-tools-plugin/), we decided to remove the External Tools plugin in Kate for the KDE 4.8 release, since it was unmaintained and we got more and more bugs that were not fixed. However, over the years, we got many requests (and complaints) to bring this plugin back to life, since it was very useful to start little helper scripts with just a shortcut, via the command line, or the menu.

The good news is that for the KDE Applications 19.12 release, the External Tools plugin will be back! And it is much improved compared to the previous version.

I've been looking at many external tools implementations, including Microsoft Visual Studio, Visual Studio Code, CLion, KDevelop, Eclipse, and Qt Creator. And I must say the implementation in Qt Creator was by far the most flexible one, so that's what I used as reference.

So let's have a look. Once you enable the plugin, a new entry "External Tools" appears in the config page. Selecting the "External Tools" entry, you will be presented with several tools by default:

<p align="center">
    <a href="/post/2019/2019-09-21-external-tools-plugin/images/external-tools.png" target="_blank"><img src="/post/2019/2019-09-21-external-tools-plugin/images/external-tools.png"></a>
</p>

Editing a tool opens a config dialog that allows fine-grained configuration of the tool:

<p align="center">
    <a href="/post/2019/2019-09-21-external-tools-plugin/images/configure-external-tool.png" target="_blank"><img src="/post/2019/2019-09-21-external-tools-plugin/images/configure-external-tool.png"></a>
</p>

As you can see, many details can be defined, namely:

- **Name**: The name of the tool, which will later appear in the menu.
- **Icon**: Optional icon that is visible in the menu.
- **Executable**: the executable. You can either provide an absolute path, or your executable must be in the PATH environment variable.
- **Arguments**: Optional arguments that are passed to the process.
- **Input**: Optional input that is passed to the process via stdin.
- **Working directory**: The working directory the tool will be started in. If empty, the working directory is set to the current document's path.
- **Mime types**: If set, the tool is active only if the current document's mime type matches.
- **Save**: When invoked, saves none, the current document, or all documents.
  <a href="/post/2019/2019-09-21-external-tools-plugin/images/external-tools-save.png" target="_blank"><img src="/post/2019/2019-09-21-external-tools-plugin/images/external-tools-save.png"></a>
- **[ ] Reload current document after execution**: Useful when the current file is modified on disk.
- **Output**: The output defines the target of stdout.
  <a href="/post/2019/2019-09-21-external-tools-plugin/images/external-tools-output.png" target="_blank"><img src="/post/2019/2019-09-21-external-tools-plugin/images/external-tools-output.png"></a>
- **Editor command**: Optional command that can be used to invoke the external tool via [Kate's built-in command line (by default shortcut F7)](https://docs.kde.org/stable5/en/applications/katepart/advanced-editing-tools-commandline.html).

The button *Defaults* is visible only for tools that are shipped with Kate. When clicked, all tool's settings reverted to default (aka factory) values.

### Support for Variables

The *Executable*, the *Arguments*, the *Input* and the *Working Directory* support variables that are expanded on tool invocation. This is indicated by the icon `{}` that appears once one of these text input fields has focus (cf. red circle):

<p align="center">
    <a href="/post/2019/2019-09-21-external-tools-plugin/images/external-tools-variables.png" target="_blank"><img src="/post/2019/2019-09-21-external-tools-plugin/images/external-tools-variables.png"></a>
</p>

Hovering over one of these text inputs also shows a tooltip with the current expanded text. Further, clicking on the `{}` action will open a dialog that lists all available variables:

<p align="center">
    <a href="/post/2019/2019-09-21-external-tools-plugin/images/external-tools-variable-chooser.png" target="_blank"><img src="/post/2019/2019-09-21-external-tools-plugin/images/external-tools-variable-chooser.png"></a>
</p>

This feature provides a lot of flexibility when defining an external tool since all variables of the form `%{...}` are expanded when the tool gets invoked. There are two kind of variables supported:

1. **%{variable-name}**
2. **%{variable-name:&lt;value&gt;}**

The first form **%{variable-name}** simply replaces the variable with its contents. For instance, the variable *%{Document:FileName}* is replaced by the current document's filename without its path.
The second form **%{variable-name:&lt;value&gt;}** gets the *&lt;value&gt;* as contents. For example, this can be used to expand an environment variable with *%{ENV:HOME}*. Or you can obtain the current date in your preferred format like *%{Date:yyyy-MM-dd}*.

Developer info: The concept of variable expansion was added to the KTextEditor framework with version 5.63 ([cf. API documentation](https://api.kde.org/frameworks/ktexteditor/html/classKTextEditor_1_1Editor.html#a8d1e545e5e9413e6a5a1bb574755c295)).


Supported variables include:

- **Document:FileBaseName**: File base name without path and suffix of the current document.
- **Document:FileExtension**: File extension of the current document.
- **Document:FileName**: File name without path of the current document.
- **Document:FilePath**: Full path of the current document including the file name.
- **Document:Text**: Contents of the current document.
- **Document:Path**: Full path of the current document excluding the file name.
- **Document:NativeFilePath**: Full document path including file name, with native path separator (backslash on Windows).
- **Document:NativePath**: Full document path excluding file name, with native path separator (backslash on Windows).
- **Document:Cursor:Line**: Line number of the text cursor position in current document (starts with 0).
- **Document:Cursor:Column**: Column number of the text cursor position in current document (starts with 0).
- **Document:Cursor:XPos**: X component in global screen coordinates of the cursor position.
- **Document:Cursor:YPos**: Y component in global screen coordinates of the cursor position.
- **Document:Selection:Text**: Text selection of the current document.
- **Document:Selection:StartLine**: Start line of selected text of the current document.
- **Document:Selection:StartColumn**: Start column of selected text of the current document.
- **Document:Selection:EndLine**: End line of selected text of the current document.
- **Document:Selection:EndColumn**: End column of selected text of the current document.
- **Document:RowCount**: Number of rows of the current document.
- **Date:Locale**: The current date in current locale format.
- **Date:ISO**: The current date (ISO).
- **Date:&lt;value&gt;**: The current date ([QDate formatstring](https://doc.qt.io/qt-5/qdate.html#toString)).
- **Time:Locale**: The current time in current locale format.
- **Time:ISO**: The current time (ISO).
- **Time:&lt;value&gt;**: The current time ([QTime formatstring](https://doc.qt.io/qt-5/qtime.html#toString)).
- **ENV:&lt;value&gt;**: Access to environment variables.
- **JS:&lt;expression&gt;**: Evaluate simple JavaScript statements.
- **UUID**: Generate a new UUID.

### List of Default Tools

Some tools are shipped by default. These tools mostly came to my mind, but if you have more useful tools please let us know at [kwrite-devel@kde.org](mailto:kwrite-devel@kde.org) so that we can add them to this list.

All default tools are visible in the list view by default.
However, you can change all of them as well to your liking, change categories, or even delete them.
Deleted tools can be added back again by clicking on the *Add...* button as follows:

<p align="center">
    <a href="/post/2019/2019-09-21-external-tools-plugin/images/default-tools.png" target="_blank"><img src="/post/2019/2019-09-21-external-tools-plugin/images/default-tools.png"></a>
</p>

The tools currently shipped by default include:

#### git-cola
git-cola is a graphical git client that enables you to easily stage and commit changes. If installed, it is available also through the command line with "git-cola".

- Name: git-cola
- Icon: git-cola
- Executable: git-cola
- Arguments: -r %{Document:Path}
- Editor command: git-cola

#### gitk
gitk is a git client as well that allows to nicely visualize the git history.

- Name: gitk
- Icon: git-gui
- Executable: gitk
- Working directory: %{Document:Path}
- Editor command: gitk

#### git blame
Starts git blame to easily follow git changes in the current file.

- Name: git blame
- Executable: git
- Arguments: gui blame %{Document:FileName}
- Save: Current Document
- Working directory: %{Document:Path}
- Editor command: git-blame

#### Run Shell Script
Starts an external konsole in which the current document is executed.
The script needs to state the interpreter in the first line via a shebang *#!/path/interpreter*.

- Name: Run Shell Script
- Icon: system-run
- Executable: konsole
- Arguments: -e sh -c "cd %{Document:Path} && pwd && chmod -vc a+x %{Document:FileName} && ./%{Document:FileName} ; echo Press any key to continue. && read -n 1"
- Working directory: %{Document:Path}
- Save: Current Document
- Editor command: run-script

#### Google Selected Text
Search in google for the selected text.

- Name: Google Selected Text
- Icon: globe
- Executable: xdg-open
- Arguments: "https://www.google.com/search?q=%{Document:Selection:Text}"
- Editor command: google

#### Insert UUID
Inserts a new UUID each time this action is invoked.

- Name: Insert UUID
- Executable: echo
- Arguments: %{UUID}
- Output: Insert at Cursor Position
- Editor command: uuid

#### Clang Format Full File
Runs clang-format on the current file on disk. The document is reloaded afterwards.

- Name: Clang Format Full File
- Executable: clang-format
- Arguments: -i %{Document:FileName}
- Working directory: %{Document:Path}
- Save: Current Document
- Reload: yes
- Editor command: clang-format-file

#### Clang Format Selected Text
Runs clang-format just on the selected text in the current document.

- Name: Clang Format Selected Text
- Executable: clang-format
- Arguments: -assume-fileName: %{Document:FileName}
- Working directory: %{Document:Path}
- Input: %{Document:Selection:Text}
- Output: Replace Selected Text
- Editor command: clang-format-selection

#### Qt Quick 2 Preview (qmlscene)
Previews the current qml file in qmlscene.

- Name: Qt Quick 2 Preview (qmlscene)
- Executable: qmlscene
- Arguments: %{Document:FileName}
- Save: Current Document
- Working directory: %{Document:Path}
- Editor command: qml-preview

### Join the Discussion

Feel free to join the discussion at the [KDE reddit](https://www.reddit.com/r/kde/comments/d7fj90/kate_external_tools_plugin_is_back/).
