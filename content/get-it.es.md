---
author: Christoph Cullmann
date: 2010-07-09 14:40:05
title: Obtener Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux y Unix

* Instalar Kate/KWrite [desde su distribución](http://www.kde.org/download/distributions.php).

* [Compilar](/build-it/#linux) el código fuente.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

* [Kate en la Windows Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

* [Kate mediante Chocolatey](https://chocolatey.org/packages/kate)

* [Instalador de la versión de Kate (64 bits)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/)

* [Instalador de la versión de Kate de cada noche (64 bits)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/)

* [Compilar](/build-it/#windows) el código fuente.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/OS_X_El_Capitan_logo.svg_.png" >}}

### macOS

* [Instalador de la versión de Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/)

* [Instalador de la versión de Kate de cada noche](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/)

* [Compilar](/build-it/#mac) el código fuente.