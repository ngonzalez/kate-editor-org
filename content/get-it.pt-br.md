---
author: Christoph Cullmann
date: 2010-07-09 14:40:05
title: Baixe o Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux e Unix

* Instale o Kate/KWrite [de sua distribuição](http://www.kde.org/download/distributions.php).

* [Compile-o](/build-it/#linux) a partir do código-fonte.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

* [Kate na Windows Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

+ [Kate via Chocolatey](https://chocolatey.org/packages/kate)

* [Instalador (64bits) da versão lançada do Kate](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/)

* [Instalador (64bits) da versão noturna do Kate](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/)

* [Compile-o](/build-it/#windows) a partir do código-fonte.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/OS_X_El_Capitan_logo.svg_.png" >}}

### macOS

* [Instalador da versão lançada do Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/)

* [Instalador da versão lançada do Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/)

* [Compile-o](/build-it/#mac) a partir do código-fonte.