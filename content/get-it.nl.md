---
author: Christoph Cullmann
date: 2010-07-09 14:40:05
title: Kate verkrijgen
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux & Unices

* Kate/KWrite installeren [uit uw distributie](http://www.kde.org/download/distributions.php).

* [Bouw het](/build-it/#linux) uit broncode.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

* [Kate in de Windows Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

+ [Kate via Chocolatey](https://chocolatey.org/packages/kate)

* [Kate uitgave (64bit) installatieprogramma](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/)

* [Kate nachtelijk (64bit) installatieprogramma](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/)

* [Bouw het](/build-it/#windows) uit broncode.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/OS_X_El_Capitan_logo.svg_.png" >}}

### macOS

* [Kate uitgave installatieprogramma](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/)

* [Kate nachtelijk installatieprogramma](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/)

* [Bouw het](/build-it/#mac) uit broncode.