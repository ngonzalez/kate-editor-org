---
author: Christoph Cullmann
date: 2010-07-09 14:40:05
title: "\u0391\u03C0\u03BF\u03BA\u03C4\u03AE\u03C3\u03C4\u03B5 \u03C4\u03BF Kate"
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux & Unices

* Εγκατάσταση των Kate/KWrite [από τη διανομή σας](http://www.kde.org/download/distributions.php).

* [Κατασκευάστε το](/build-it/#linux) από τον πηγαίο κώδικα.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

* [Το Kate στο Windows Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

* [Το Kate μέσω Chocolatey](https://chocolatey.org/packages/kate)

* [Πρόγραμμα εγκατάστασης έκδοσης Kate (64bit)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/)

* [Πρόγραμμα εγκατάσταης Kate nightly (64bit)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/)

* [Κατασκευάστε το](/build-it/#windows) από τον πηγαίο κώδικα.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/OS_X_El_Capitan_logo.svg_.png" >}}

### macOS

* [Πρόγραμμα εγκατάστασης έκδοσης του Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/)

* [Πρόγραμμα εγκατάστασης Kate nightly](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/)

* [Κατασκευάστε το](/build-it/#mac) από τον πηγαίο κώδικα.