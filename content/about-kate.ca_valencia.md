---
title: Features

author: Christoph Cullmann

date: 2010-07-09T08:40:19+00:00
---

## Característiques de l'aplicació

![Captura de pantalla que mostra la característica de la finestra dividida i
un connector de terminal](/images/kate-window.png)

+ Visualització i edició de múltiples documents a la vegada, dividint la
finestra horitzontalment i verticalment + Molts connectors: [Terminal
incrustat](https://konsole.kde.org), connector SQL, connector de
construcció, connector del GDB, Substitució a fitxers, i més + Interfície
multidocument (MDI) + Suport de sessions

## Característiques generals

![Captura de pantalla que mostra la característica de cerca i
substitució](/images/kate-search-replace.png)

+ Admet codificacions (Unicode i molts altres) + Admet la representació de
text bidireccional + Admet els finals de línia (Windows, Unix, Mac),
incloent la detecció automàtica + Transparència de xarxa (obri fitxers
remots)  + És ampliable mitjançant la creació de scripts

## Característiques avançades d'edició

![Captura de pantalla de la vora del Kate amb el número de línia i un
punt](/images/kate-border.png)

+ Sistema de punts (també admet: punts d'interrupció, etc.)  + Marques de
barres de desplaçament + Indicadors de modificació de línia + Números de
línia + Plegat de codi

## Ressaltat de sintaxi

![Captura de pantalla de les característiques de ressaltat de sintaxi del
Kate](/images/kate-syntax.png)

+ Admet el ressaltat de més de 300 llenguatges + Coincidència de parèntesis
+ Verificació ortogràfica al vol intel·ligent + Ressaltat de paraules
seleccionades

## Característiques de programació

![Captura de pantalla de les característiques de
programació](/images/kate-programming.png)

+ Sagnat automàtic que admet scripts + Gestió intel·ligent de comentat i
descomentat + Compleció automàtica amb consells d'arguments + Mode d'entrada
del Vi + Mode de selecció de bloc rectangular

## Cerca i substitució

![Captura de pantalla de la característica de cerca incremental del
Kate](/images/kate-search.png)

+ Cerca incremental, coneguda també com &#8220;cerca en teclejar&#8221; +
Admet la cerca i substitució multilínia + Admet expressions regulars + Cerca
i substitució en múltiples fitxers oberts o en fitxers al disc

## Còpia de seguretat i restauració

![Captura de pantalla de la característica de recuperació d'una fallada del
Kate](/images/kate-crash.png)

+ Còpies de seguretat en guardar + Fitxers d'intercanvi per a recuperar les
dades en cas de fallada del sistema + Sistema de desfer/refer
