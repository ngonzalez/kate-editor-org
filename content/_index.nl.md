---
layout: index
---
Kate is een tekstbewerker van meerdere documenten en onderdeel van [KDE](https://kde.org) sinds uitgave 2.2. Omdat het behoort tot de [KDE toepassingen/applications](https://kde.org/applications), wordt Kate geleverd met netwerktransparantie , evenals integratie met de geweldige mogelijkheden van KDE. Kies het voor bekijken van HTML broncode uit konqueror, bewerken van configuratiebestanden, schrijven van nieuwe toepassingen of elk andere taak met tekstverwerking. U gebruikt slechts één actief exemplaar van Kate. [Leer meer...](/about/)

![Schermafdruk van Kate die meerdere documenten toont en bezig met bouwen van
terminalemulator](/images/kate-window.png)