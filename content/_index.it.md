---
layout: index
---
Kate è un editor multi-documento e fa parte di [KDE](https://kde.org) fin dal rilascio della versione 2.2. Essendo un'[applicazione KDE](https://kde.org/applications), Kate viene fornito con la trasparenza di rete, così come con l'integrazione alle fantastiche funzionalità di KDE. Usalo per visualizzare il codice HTML da Konqueror, modificare file di configurazione, scrivere nuove applicazioni o qualsiasi altra operazione di modifica del testo. Ti basta avviare solo un'istanza di Kate. [Ulteriori informazioni...](/about/)

![Schermata di Kate che mostra più documenti e l'emulatore di terminale per la
compilazione](/images/kate-window.png)