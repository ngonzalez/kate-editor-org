---
author: Christoph Cullmann
date: 2010-07-09 08:40:19
title: Funcionalidades
---
## Funcionalidades de la aplicación

![Captura de pantalla que muestra la funcionalidad de división de pantalla de Kate y el complemento de terminal](/images/kate-window.png)

* Ver y editar múltiples documentos a la vez dividiendo la ventana horizontal y verticalmente.

* Gran cantidad de complementos: [terminal integrado](https://konsole.kde.org), complemento SQL, complemento de compilación, complemento GDB, sustituir en archivos, y más.

* Interfaz multidocumento (MDI).

* Uso de sesiones.

## Funcionalidades generales

![Captura de pantalla que muestra la funcionalidad de buscar y sustituir de Kate](/images/kate-search-replace.png)

* Uso de codificación de texto (Unicode y muchas más).

* Permite mostrar texto bidireccional.

* Varios tipos de final de línea (Windows, Unix, Mac), permitiendo su detección automática.

* Transparencia de red (apertura de archivos remotos).

* Se puede extender mediante «scripts».

## Funciones de edición avanzada

![Captura de pantalla del borde de Kate con números de líneas y marcadores](/images/kate-border.png)

* Sistema de marcadores (también permite puntos de interrupción, etc.).

* Marcas en la barra de desplazamiento.

* Indicadores de líneas modificadas.

* Números de línea.

* Plegado de código.

## Resaltado de sintaxis

![Captura de pantalla que muestra las funcionalidades de resaltado de sintaxis de Kate](/images/kate-syntax.png)

* Permite resaltado de sintaxis de unos 300 lenguajes.

* Emparejamiento de paréntesis.

* Comprobación ortográfica inteligente instantánea.

* Resaltado de las palabras seleccionadas.

## Funcionalidades de programación

![Captura de pantalla de las funcionalidades de programación de Kate](/images/kate-programming.png)

* Sangrado automático programable mediante «scripts».

* Función inteligente para crear y eliminar comentarios.

* Terminación de palabras automática con sugerencias de argumentos.

* Modo de entrada Vi

* Modo de selección de bloques rectangulares.

## Buscar y sustituir

![Captura de pantalla que muestra la función de búsqueda incremental de Kate](/images/kate-search.png)

* Búsqueda incremental, también conocida como «buscar al escribir».

* Permite buscar y sustituir varias líneas.

* Permite usar expresiones regulares.

* Buscar y sustituir en varios archivos abiertos o en disco.

## Crear y restaurar copias de seguridad

![Captura de pantalla de la función de recuperación de Kate después de un cuelgue de la aplicación](/images/kate-crash.png)

* Copias de seguridad al guardar.

* Archivos de intercambio para la recuperación de datos tras un fallo del sistema.

* Sistema para deshacer y rehacer.