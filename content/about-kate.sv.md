---
author: Christoph Cullmann
date: 2010-07-09 08:40:19
title: Funktioner
---
## Programfunktioner

![Skärmbild som visar Kates fönsterdelningsfunktion och terminalinsticksprogram](/images/kate-window.png)

* Visa och redigera flera dokument samtidigt, genom att dela fönstret horisontellt och vertikalt

* Många insticksprogram: [inbäddad terminal](https://konsole.kde.org), SQL-insticksprogram, bygginsticksprogram, GDB-insticksprogram, Ersätt i filer, med mera

* Flerdokumentgränssnitt (MDI)

* Sessionsstöd

## Allmänna funktioner

![Skärmbild som visar Kates söknings- och ersättningsfunktion](/images/kate-search-replace.png)

* Kodningsstöd (Unicode och många andra)

* Stöd för bidirektionell textåtergivning

* Stöd för radslut (Windows, Unix, Mac), inklusive automatisk detektering

* Nätverkstransparens (öppna fjärrfiler)

* Utökningsbart via skript

## Avancerade redigeringsfunktioner

![Skärmbild av Kates kant med radnummer och bokmärke](/images/kate-border.png)

* Bokmärkessystem (stöder också brytpunkter, etc.)

* Rullningslistmarkeringar

* Indikering av radändringar

* Radnummer

* Kodvikning

## Syntaxfärgläggning

![Skärmbild av Kates syntaxfärgläggningsfunktion](/images/kate-syntax.png)

* Färgläggningsstöd för mer än 300 språk

* Parentesmatchning

* Smart stavningskontroll i farten

* Färgläggning av utvalda ord

## Programmeringsfunktioner

![Skärmbild av Kates programmingsfunktioner](/images/kate-programming.png)

* Skriptbar automatisk indentering

* Smart hantering för kommentera och avkommentera

* Automatisk komplettering med argumenttips

* VI-inmatningsläge

* Rektangulärt blockmarkeringsläge

## Sök och ersätt

![Skärmbild av Kates inkrementella sökfunktion](/images/kate-search.png)

* Inkrementell sökning, också känt som &#8220;sök medan du skriver&#8221;

* Stöd för sök och ersätt av flera rader

* Stöd för reguljära uttryck

* Sök och ersätt i flera öppna filer eller filer på disk

## Säkerhetskopiera och återställ

![Skärmbild av Kates funktion för kraschåterställning](/images/kate-crash.png)

* Säkerhetskopior vid spara

* Växlingsfiler för att återställa data vid systemkrascher

* System för ångra och gör om