---
author: Christoph Cullmann
date: 2010-07-09 14:40:05
title: Obtenir Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux & Unix

* Installer Kate / KWrite [à partir de votre distribution] (http://www.kde.org/download/distributions.php).

* [Compiler Kate](/build-it/#linux) à partir des sources.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

* [Kate dans la boutique « Windows »] (https://www.microsoft.com/store/apps/9NWMW7BB59HW)

* [Kate par « Chocolatey »] (https://chocolatey.org/packages/kate)

* [Installateur de la version Kate (64bit) installer](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/)

* [Installateur de Kate en version de développement (64bit)] (https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/)

* [Compiler Kate](/build-it/#windows) à partir des sources.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/OS_X_El_Capitan_logo.svg_.png" >}}

### MacOS

* [Installateur de la version Kate] (https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/)

* [Installateur de la version de développement de Kate] (https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/)

* [Compiler Kate](/build-it/#mac) à partir des sources.