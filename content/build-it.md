---
title: Build It
author: Christoph Cullmann
date: 2016-06-13T15:15:02+00:00
---

# Building Kate from Sources on Linux {#linux}

Right now, Kate's source code is located on the [invent.kde.org](https://invent.kde.org/utilities/kate) GitLab instance.

This how-to explains how to build Kate without touching your stable KDE installation.

### Install dependencies

Make sure you have the following packages installed: <strong>git</strong> and the <strong>KF5 development package</strong> (and CMake).

* openSUSE
{{< highlight bash >}}
sudo zypper in libgit2-devel gettext-tools extra-cmake-modules \
  libQt5Widgets-devel libQt5Xml-devel libQt5Test-devel \
  libQt5Gui-devel libQt5DBus-devel libQt5Concurrent-devel \
  libqt5-qtscript-devel libQt5Sql-devel ktexteditor-devel \
  kactivities5-devel kiconthemes-devel kguiaddons-devel \
  kcrash-devel kdoctools-devel kinit-devel kwindowsystem-devel \
  kdbusaddons-devel kwallet-devel plasma-framework-devel \
  kitemmodels-devel knotifications-devel threadweaver-devel knewstuff-devel
{{< / highlight >}}

* Ubuntu and derivates
{{< highlight bash >}}
sudo apt install qtbase5-dev qtscript5-dev
extra-cmake-modules plasma-framework-dev libkf5crash-dev \
  libkf5config-dev libkf5activities-dev kinit-dev kdoctools-dev \
  libkf5i18n-dev libkf5parts-dev libkf5guiaddons-dev libkf5iconthemes-dev \
  libkf5jobwidgets-dev kio-dev libkf5texteditor-dev \
  libkf5windowsystem-dev libkf5xmlgui-dev libgit2-dev libkf5wallet-dev \
  libkf5service-dev libkf5itemmodels-dev \
  libkf5notifications-dev libkf5threadweaver-dev libkf5newstuff-dev
{{< / highlight >}}

* Fedora/RHEL/CentOS
{{< highlight bash >}}
sudo yum install git cmake gcc-g++ extra-cmake-modules qt5-qtbase-devel \
  qt5-qtscript-devel kf5-kiconthemes-devel kf5-kxmlgui-devel \
  kf5-kwindowsystem-devel kf5-ktexteditor-devel kf5-kparts-devel \
  kf5-kguiaddons-devel kf5-kinit-devel kf5-kdoctools-devel kf5-kcrash-devel \
  kf5-kactivities-devel kf5-kdbusaddons-devel kf5-kwallet-devel \
  kf5-knewstuff-devel kf5-threadweaver-devel kf5-kitemmodels-devel \
  kf5-plasma-devel kf5-knotifications-devel libgit2-devel
{{< / highlight >}}

### Prepare the Kate sources

* create and change into a KDE development directory:
{{< highlight bash >}}
mkdir -p ~/kde/usr
cd ~/kde
{{< / highlight >}}

* get a copy of the Kate code:
{{< highlight bash >}}
git clone https://invent.kde.org/utilities/kate.git
cd kate
{{< / highlight >}}

<strong>NOTE:</strong> If you want to build KTextEditor (the editor component), you need to follow exactly the same steps, but replace &#8216;kate&#8217; with &#8216;ktexteditor&#8217; everywhere.

Your copy of Kate contains all of the Kate code, i.e.: Kate, KWrite and all bundled plugins.

### Configure Kate

* create and change into a build directory inside the kate directory for compilation:
{{< highlight bash >}}
mkdir build
cd build
{{< / highlight >}}

* run the configure process with cmake:
 - For just using Kate, build it optimized but with debugging symbols to get useful backtraces for bugreports:
{{< highlight bash >}}
cmake .. -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_INSTALL_PREFIX=~/kde/usr \
  -DCMAKE_PREFIX_PATH=~/kde/usr
{{< / highlight >}}
 - For developing Kate, build it with debug support (if you have problems using gdb because of optimizations, swap Debug with DebugFull):
{{< highlight bash >}}
cmake .. -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=~/kde/usr \
  -DCMAKE_PREFIX_PATH=~/kde/usr
{{< / highlight >}}

### Build & Install

* compile Kate:
{{< highlight bash >}}
make
{{< / highlight >}}

* finally install Kate:
{{< highlight bash >}}
make install
{{< / highlight >}}
This installs Kate locally into the separate directory <code>~/kde/usr</code>, so that your global KDE installation will not be touched at all.

### Run the self-compiled Kate

Now you can run the compiled Kate version from you shell via <code>kate</code> after sourcing the prefix.sh in your build directory.
{{< highlight bash >}}
. prefix.sh
kate
{{< / highlight >}}

### Keeping Kate up-to-date?

You can keep your copy up-to-date by typing:
{{< highlight bash >}}
cd ~/kde/kate
git pull --rebase
{{< / highlight >}}

### Contribute back!

Feel free to create a merge request at [invent.kde.org](https://invent.kde.org/utilities/kate/merge_requests).
For more information see [Join Us](/join-us/).

### Get Support?

If you have questions you can ask them on our mailing list [kwrite-devel@kde.org](mailto:kwrite-devel@kde.org) and/or join #kate on irc.kde.org.

For more information see [Get Help](/support/).

# Building Kate from Sources on Windows {#mac}

To build the KF5 based Kate/KWrite you can follow the guide on [KDE on Windows](https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source/Windows).

# Building Kate from Sources on macOS {#mac}

To build the KF5 based Kate/KWrite you can follow the guide on [KDE on macOS](https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source/Mac).
