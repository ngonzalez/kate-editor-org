---
title: The Team
author: Christoph Cullmann
date: 2010-07-09T09:03:57+00:00
---

### Who are the Kate & KWrite contributors?

[Kate & KWrite](https://invent.kde.org/utilities/kate) and the underlying [KTextEditor](https://invent.kde.org/frameworks/ktexteditor) & [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting) frameworks are created by a group of volunteers around the world.
The same is true for [this website](https://invent.kde.org/websites/kate-editor-org).

Below is a periodically updated list of the contributors to the Git repositories containing the above named software components & website.
As this list is purely based on the Git history of our repositories, it will be incomplete for otherwise submitted patches, etc.

The list is sorted by number of commits done by the individual contributors.
This is by no means the best measure of the impact of their contributions, but gives some rough estimation about their level of involvement.

### Don't forget the KDE Community!

Beside these explicitly named contributors to our text editor related components, a large portion of work is done by other members of the much broader [KDE community](https://kde.org/).

This includes crucial work like:

* developing the foundations we use, like KDE Frameworks
* doing all internationalization and translation work for our projects
* releasing our stuff
* maintaining our infrastructure (GitLab, CI, ...)
* supporting developer sprints

If you are not interested in [joining our text editor related team](/join-us/), please take a look if you want to [contribute to KDE](https://community.kde.org/Get_Involved).
The KDE community is a very welcoming bunch of people.

If you are not able to contribute, you might be interested to [donate](https://kde.org/donations).
Whereas these donations are not directly targeted at specific development, they help to keep the overall KDE community going.
For example some Kate related coding sprints were funded by the [KDE e.V.](https://ev.kde.org) based on these donations.

### Our 552 current & past contributors

* Christoph Cullmann <!-- 5349 -->
* Dominik Haumann <!-- 2543 -->
* Joseph Wenninger <!-- 538 -->
* Erlend Hamberg <!-- 514 -->
* Anders Lund <!-- 513 -->
* Hamish Rodda <!-- 482 -->
* Simon St James <!-- 456 -->
* Kåre Särs <!-- 432 -->
* Alex Turbov <!-- 397 -->
* Volker Krause <!-- 363 -->
* Milian Wolff <!-- 361 -->
* Laurent Montel <!-- 342 -->
* Michal Humpula <!-- 332 -->
* David Nolden <!-- 269 -->
* Bernhard Beschow <!-- 219 -->
* Shaheed Haque <!-- 208 -->
* David Faure <!-- 171 -->
* Mark Nauwelaerts <!-- 166 -->
* Pascal Létourneau <!-- 165 -->
* Miquel Sabaté <!-- 163 -->
* Nibaldo González <!-- 162 -->
* Albert Astals Cid <!-- 159 -->
* Sven Brauch <!-- 155 -->
* Burkhard Lück <!-- 134 -->
* Friedrich W. H. Kossebau <!-- 122 -->
* Sebastian Pipping <!-- 121 -->
* Pino Toscano <!-- 107 -->
* Yuri Chornoivan <!-- 102 -->
* T.C. Hollingsworth <!-- 96 -->
* Jonathan Poelen <!-- 94 -->
* Christian Ehrlicher <!-- 93 -->
* Dirk Mueller <!-- 93 -->
* Robin Pedersen <!-- 93 -->
* Wilbert Berendsen <!-- 93 -->
* Pablo Martín <!-- 88 -->
* Michel Ludwig <!-- 87 -->
* Matthew Woehlke <!-- 84 -->
* loh tar <!-- 81 -->
* John Firebaugh <!-- 80 -->
* Alex Neundorf <!-- 74 -->
* Kevin Funk <!-- 72 -->
* Svyatoslav Kuzmich <!-- 66 -->
* Adrian Lungu <!-- 62 -->
* Christian Couder <!-- 56 -->
* Stephan Binner <!-- 56 -->
* Christoph Feck <!-- 55 -->
* Stephan Kulow <!-- 55 -->
* Mirko Stocker <!-- 54 -->
* Jesse Yurkovich <!-- 52 -->
* Alex Merry <!-- 51 -->
* Thomas Friedrichsmeier <!-- 50 -->
* Vegard Øye <!-- 49 -->
* Malcolm Hunter <!-- 46 -->
* Alexander Neundorf <!-- 44 -->
* Gregor Mi <!-- 42 -->
* Montel Laurent <!-- 42 -->
* Rafael Fernández López <!-- 38 -->
* Marco Mentasti <!-- 35 -->
* Simon Huerlimann <!-- 35 -->
* Carl Schwan <!-- 34 -->
* Urs Wolfer <!-- 33 -->
* Allen Winter <!-- 32 -->
* Aleix Pol <!-- 31 -->
* Phil Schaf <!-- 30 -->
* Jakob Petsovits <!-- 29 -->
* Andrew Coles <!-- 28 -->
* Gerald Senarclens de Grancy <!-- 28 -->
* Adriaan de Groot <!-- 27 -->
* Chusslove Illich <!-- 27 -->
* Thomas Fjellstrom <!-- 27 -->
* Andreas Hartmetz <!-- 26 -->
* Martijn Klingens <!-- 26 -->
* Ahmad Samir <!-- 24 -->
* André Wöbbeking <!-- 24 -->
* Alex Richardson <!-- 22 -->
* Jarosław Staniek <!-- 22 -->
* Patrick Spendrin <!-- 22 -->
* Frederik Schwarzer <!-- 21 -->
* Nicolas Goutte <!-- 21 -->
* Bram Schoenmakers <!-- 20 -->
* Simon Hausmann <!-- 20 -->
* Tomáš Trnka <!-- 20 -->
* Adrián Chaves Fernández (Gallaecio) <!-- 19 -->
* Aurélien Gâteau <!-- 19 -->
* Jan Paul Batrina <!-- 19 -->
* Aaron J. Seigo <!-- 18 -->
* Andreas Kling <!-- 18 -->
* Arto Hytönen <!-- 17 -->
* Clarence Dang <!-- 17 -->
* Niko Sams <!-- 17 -->
* Paul Giannaros <!-- 17 -->
* Andrew Paseltiner <!-- 16 -->
* Brian Anderson <!-- 16 -->
* Ian Reinhart Geiser <!-- 16 -->
* Frederik Gladhorn <!-- 15 -->
* Kevin Ottens <!-- 15 -->
* Matt Rogers <!-- 15 -->
* Thiago Macieira <!-- 14 -->
* Daniel Naber <!-- 13 -->
* Eike Hein <!-- 13 -->
* Jan Villat <!-- 13 -->
* Kai Uwe Broulik <!-- 13 -->
* Luca Beltrame <!-- 13 -->
* Luigi Toscano <!-- 13 -->
* Waldo Bastian <!-- 13 -->
* Johannes Sixt <!-- 12 -->
* Jonathan Riddell <!-- 12 -->
* José Pablo Ezequiel Fernández <!-- 12 -->
* Leo Savernik <!-- 12 -->
* Andreas Cord-Landwehr <!-- 11 -->
* Andreas Pakulat <!-- 11 -->
* Andrey Matveyakin <!-- 11 -->
* Chusslove Illich (Часлав Илић) <!-- 11 -->
* Daan De Meyer <!-- 11 -->
* Diego Iastrubni <!-- 11 -->
* Tobias Koenig <!-- 11 -->
* Anthony Fieroni <!-- 10 -->
* Christoph Roick <!-- 10 -->
* Eduardo Robles Elvira <!-- 10 -->
* Harald Fernengel <!-- 10 -->
* Heiko Becker <!-- 10 -->
* Jiri Pinkava <!-- 10 -->
* Jonathan Schmidt-Dominé <!-- 10 -->
* Matthias Kretz <!-- 10 -->
* Philipp A <!-- 10 -->
* Trevor Blight <!-- 10 -->
* Wang Kai <!-- 10 -->
* Alexander Potashev <!-- 9 -->
* Christophe Giboudeaux <!-- 9 -->
* Ellis Whitehead <!-- 9 -->
* Harsh Kumar <!-- 9 -->
* Jaime Torres <!-- 9 -->
* Lasse Liehu <!-- 9 -->
* Maks Orlovich <!-- 9 -->
* Oswald Buddenhagen <!-- 9 -->
* Tomaz Canabrava <!-- 9 -->
* Andras Mantia <!-- 8 -->
* Christian Loose <!-- 8 -->
* David Edmundson <!-- 8 -->
* David Palser <!-- 8 -->
* Dmitry Risenberg <!-- 8 -->
* Frank Osterfeld <!-- 8 -->
* John Layt <!-- 8 -->
* John Tapsell <!-- 8 -->
* Oliver Kellogg <!-- 8 -->
* Primoz Anzur <!-- 8 -->
* Ralf Habacker <!-- 8 -->
* René J.V. Bertin <!-- 8 -->
* flying sheep <!-- 8 -->
* Abhishek Patil <!-- 7 -->
* Anne-Marie Mahfouf <!-- 7 -->
* Bastian Holst <!-- 7 -->
* David Jarvie <!-- 7 -->
* Filip Gawin <!-- 7 -->
* George Staikos <!-- 7 -->
* Jeroen Wijnhout <!-- 7 -->
* John Salatas <!-- 7 -->
* Kazuki Ohta <!-- 7 -->
* Martin Walch <!-- 7 -->
* Matt Broadstone <!-- 7 -->
* Nate Graham <!-- 7 -->
* Orgad Shaneh <!-- 7 -->
* Peter Oberndorfer <!-- 7 -->
* Ryan Cumming <!-- 7 -->
* Yury G. Kudryashov <!-- 7 -->
* Adam Treat <!-- 6 -->
* Charles Samuels <!-- 6 -->
* David Schulz <!-- 6 -->
* Dawit Alemayehu <!-- 6 -->
* Gleb Popov <!-- 6 -->
* Hannah von Reth <!-- 6 -->
* Ian Wakeling <!-- 6 -->
* Jaison Lee <!-- 6 -->
* Jos van den Oever <!-- 6 -->
* Lukáš Tinkl <!-- 6 -->
* Martin T. H. Sandsmark <!-- 6 -->
* Massimo Callegari <!-- 6 -->
* Michael Jansen <!-- 6 -->
* Méven Car <!-- 6 -->
* Nathaniel Graham <!-- 6 -->
* Nick Shaforostoff <!-- 6 -->
* R.J.V. Bertin <!-- 6 -->
* Rafał Rzepecki <!-- 6 -->
* Safa AlFulaij <!-- 6 -->
* Samuel Gaist <!-- 6 -->
* Shubham Jangra <!-- 6 -->
* Weng Xuetian <!-- 6 -->
* York Xiang <!-- 6 -->
* Allan Sandfeld Jensen <!-- 5 -->
* Andreas Holzammer <!-- 5 -->
* Arno Rehn <!-- 5 -->
* Bernd Gehrmann <!-- 5 -->
* Carlo Segato <!-- 5 -->
* Carsten Pfeiffer <!-- 5 -->
* Charles Vejnar <!-- 5 -->
* Daniel Levin <!-- 5 -->
* David Herberth <!-- 5 -->
* David Redondo <!-- 5 -->
* Harri Porten <!-- 5 -->
* Harsh Chouraria J <!-- 5 -->
* Helio Castro <!-- 5 -->
* Heng Liu <!-- 5 -->
* Holger Danielsson <!-- 5 -->
* Ivan Čukić <!-- 5 -->
* Ivo Anjo <!-- 5 -->
* Michael Hansen <!-- 5 -->
* Nicolas Fella <!-- 5 -->
* Nicolás Alvarez <!-- 5 -->
* Olivier Goffart <!-- 5 -->
* Peter Kümmel <!-- 5 -->
* Richard Smith <!-- 5 -->
* Scott Wheeler <!-- 5 -->
* Stephen Kelly <!-- 5 -->
* Thomas Braun <!-- 5 -->
* andreas kainz <!-- 5 -->
* Andi Fischer <!-- 4 -->
* Andrius Štikonas <!-- 4 -->
* Ben Cooksley <!-- 4 -->
* Benjamin Meyer <!-- 4 -->
* Darío Andrés Rodríguez <!-- 4 -->
* Dominique Devriese <!-- 4 -->
* Emeric Dupont <!-- 4 -->
* Evgeniy Ivanov <!-- 4 -->
* Flavio Castelli <!-- 4 -->
* Grzegorz Szymaszek <!-- 4 -->
* Guo Yunhe <!-- 4 -->
* Helio Chissini de Castro <!-- 4 -->
* Luboš Luňák <!-- 4 -->
* Michael Pyne <!-- 4 -->
* Nadeem Hasan <!-- 4 -->
* Nikita Sirgienko <!-- 4 -->
* Rolf Eike Beer <!-- 4 -->
* Stefan Asserhäll <!-- 4 -->
* Thomas Schoeps <!-- 4 -->
* Toshitaka Fujioka <!-- 4 -->
* Aaron Puchert <!-- 3 -->
* Alex Crichton <!-- 3 -->
* Bernhard Loos <!-- 3 -->
* Boris Egorov <!-- 3 -->
* Dave Corrie <!-- 3 -->
* David Leimbach <!-- 3 -->
* Davide Bettio <!-- 3 -->
* Dāvis Mosāns <!-- 3 -->
* Ernesto Castellotti <!-- 3 -->
* Harald Sitter <!-- 3 -->
* Helge Deller <!-- 3 -->
* Holger Schröder <!-- 3 -->
* Ignacio Castaño Aguado <!-- 3 -->
* Ilya Konstantinov <!-- 3 -->
* Jacob Rideout <!-- 3 -->
* Laurence Withers <!-- 3 -->
* Marcus Camen <!-- 3 -->
* Markus Meik Slopianka <!-- 3 -->
* Martin Kostolný <!-- 3 -->
* Michael Goffioul <!-- 3 -->
* Mickael Marchand <!-- 3 -->
* Mike Harris <!-- 3 -->
* Raymond Wooninck <!-- 3 -->
* Rex Dieter <!-- 3 -->
* Reza Arbab <!-- 3 -->
* Richard Dale <!-- 3 -->
* Richard J. Moore <!-- 3 -->
* Robert Knight <!-- 3 -->
* Sergio Martins <!-- 3 -->
* Simon Persson <!-- 3 -->
* Vladimir Prus <!-- 3 -->
* Willy De la Court <!-- 3 -->
* Wolfgang Bauer <!-- 3 -->
* Ömer Fadıl USTA <!-- 3 -->
* Alexander Dymo <!-- 2 -->
* Alexander Lohnau <!-- 2 -->
* Alexander Zhigalin <!-- 2 -->
* Andrew Crouthamel <!-- 2 -->
* Antonio Larrosa Jimenez <!-- 2 -->
* Antonio Rojas <!-- 2 -->
* Arend van Beelen jr <!-- 2 -->
* Bernhard Rosenkraenzer <!-- 2 -->
* Bhushan Shah <!-- 2 -->
* Caleb Tennis <!-- 2 -->
* Casper Boemann <!-- 2 -->
* Chris Howells <!-- 2 -->
* Cornelius Schumacher <!-- 2 -->
* Craig Drummond <!-- 2 -->
* Cristian Oneț <!-- 2 -->
* Cédric Borgese <!-- 2 -->
* Dmitry Suzdalev <!-- 2 -->
* Elvis Angelaccio <!-- 2 -->
* Enrique Matías Sánchez <!-- 2 -->
* Fabian Kosmale <!-- 2 -->
* Frans Englich <!-- 2 -->
* Frerich Raabe <!-- 2 -->
* Gene Thomas <!-- 2 -->
* George Florea Bănuș <!-- 2 -->
* Gioele Barabucci <!-- 2 -->
* Gregory S. Hayes <!-- 2 -->
* Hoàng Đức Hiếu <!-- 2 -->
* Hugo Pereira Da Costa <!-- 2 -->
* Huon Wilson <!-- 2 -->
* Ivan Shapovalov <!-- 2 -->
* Jaime Torres Amate <!-- 2 -->
* Jekyll Wu <!-- 2 -->
* Jens Dagerbo <!-- 2 -->
* Julien Antille <!-- 2 -->
* Jure Repinc <!-- 2 -->
* Kurt Pfeifle <!-- 2 -->
* Leonardo Finetti <!-- 2 -->
* Marcello Massaro <!-- 2 -->
* Markus Pister <!-- 2 -->
* Maximilian Löffler <!-- 2 -->
* Michael Palimaka <!-- 2 -->
* Michel Hermier <!-- 2 -->
* Mickael Bosch <!-- 2 -->
* Miklos Marton <!-- 2 -->
* Nikolas Zimmermann <!-- 2 -->
* Petter Stokke <!-- 2 -->
* Phil Young <!-- 2 -->
* Pierre-Marie Pédrot <!-- 2 -->
* Raphael Kubo da Costa <!-- 2 -->
* Rob Buis <!-- 2 -->
* Robert Hoffmann <!-- 2 -->
* Roberto Raggi <!-- 2 -->
* Scott Lawrence <!-- 2 -->
* Sebastian Kügler <!-- 2 -->
* Sebastian Sauer <!-- 2 -->
* Sergey Kalinichev <!-- 2 -->
* Shaun Reich <!-- 2 -->
* Silas Lenz <!-- 2 -->
* Sven Leiber <!-- 2 -->
* Thomas Braxton <!-- 2 -->
* Thomas Häber <!-- 2 -->
* Thomas Leitner <!-- 2 -->
* Thomas Surrel <!-- 2 -->
* Thorsten Roeder <!-- 2 -->
* Tim Beaulen <!-- 2 -->
* Tim Hutt <!-- 2 -->
* Tobias C. Berner <!-- 2 -->
* Valentin Rouet <!-- 2 -->
* Vincent Belliard <!-- 2 -->
* Vincenzo Buttazzo <!-- 2 -->
* Volker Augustin <!-- 2 -->
* Wes H <!-- 2 -->
* Will Entriken <!-- 2 -->
* Zack Rusin <!-- 2 -->
* Алексей Шилин <!-- 2 -->
* Aaron Seigo <!-- 1 -->
* Aleix Pol Gonzalez <!-- 1 -->
* Alex Hermann <!-- 1 -->
* Alexander Volkov <!-- 1 -->
* Alexey Bogdanenko <!-- 1 -->
* Amit Kumar Jaiswal <!-- 1 -->
* Ana Beatriz Guerrero López <!-- 1 -->
* Anakim Border <!-- 1 -->
* Anders Ponga <!-- 1 -->
* Andre Heinecke <!-- 1 -->
* Andrea Canciani <!-- 1 -->
* Andrea Scarpino <!-- 1 -->
* Andreas Gratzer <!-- 1 -->
* Andreas Hohenegger <!-- 1 -->
* Andreas Simon <!-- 1 -->
* Andreas Sturmlechner <!-- 1 -->
* Andrew Chen <!-- 1 -->
* Andrey S. Cherepanov <!-- 1 -->
* Andrius da Costa Ribas <!-- 1 -->
* André Marcelo Alvarenga <!-- 1 -->
* Andy Goossens <!-- 1 -->
* Antoni Bella Pérez <!-- 1 -->
* Arctic Ice Studio <!-- 1 -->
* Arnaud Ruiz <!-- 1 -->
* Arnold Dumas <!-- 1 -->
* Arnold Krille <!-- 1 -->
* Ashish Bansal <!-- 1 -->
* Axel Kittenberger <!-- 1 -->
* Ayushmaan jangid <!-- 1 -->
* Azat Khuzhin <!-- 1 -->
* Bart Ribbers <!-- 1 -->
* Barış Metin <!-- 1 -->
* Ben Blum <!-- 1 -->
* Benjamin Buch <!-- 1 -->
* Bernd Buschinski <!-- 1 -->
* Björn Peemöller <!-- 1 -->
* Brad Hards <!-- 1 -->
* Brendan Zabarauskas <!-- 1 -->
* Bruno Virlet <!-- 1 -->
* Casper van Donderen <!-- 1 -->
* Christoph Rüßler <!-- 1 -->
* Christopher Blauvelt <!-- 1 -->
* Claudio Bantaloukas <!-- 1 -->
* Conrad Hoffmann <!-- 1 -->
* Corey Richardson <!-- 1 -->
* Cédric Pasteur <!-- 1 -->
* Dan Vrátil <!-- 1 -->
* Daniel Laidig <!-- 1 -->
* Daniel Micay <!-- 1 -->
* David Rosca <!-- 1 -->
* David Smith <!-- 1 -->
* Denis Steckelmacher <!-- 1 -->
* Diana-Victoria Tiriplica <!-- 1 -->
* Diggory Hardy <!-- 1 -->
* Dirk Rathlev <!-- 1 -->
* Duncan Mac-Vicar Prett <!-- 1 -->
* Ede Rag <!-- 1 -->
* Ederag <!-- 1 -->
* Elias Probst <!-- 1 -->
* Emanuele Tamponi <!-- 1 -->
* Emmanuel Lepage Vallee <!-- 1 -->
* Ewald Snel <!-- 1 -->
* Federico Zenith <!-- 1 -->
* Felix Yan <!-- 1 -->
* Filipe Saraiva <!-- 1 -->
* Francis Herne <!-- 1 -->
* Francois-Xavier Duranceau <!-- 1 -->
* Frederik Banning <!-- 1 -->
* Fredrik Höglund <!-- 1 -->
* Germain Garand <!-- 1 -->
* Gregor Tätzner <!-- 1 -->
* Guillermo Antonio Amaral Bastidas <!-- 1 -->
* Guillermo Molteni <!-- 1 -->
* Hartmut Goebel <!-- 1 -->
* Heinz Wiesinger <!-- 1 -->
* HeroesGrave <!-- 1 -->
* Héctor Mesa Jiménez <!-- 1 -->
* Ian Monroe <!-- 1 -->
* Ignat Semenov <!-- 1 -->
* Ivan Koveshnikov <!-- 1 -->
* Jan Grulich <!-- 1 -->
* Jan Przybylak <!-- 1 -->
* Jeremy Whiting <!-- 1 -->
* Jesse Crossen <!-- 1 -->
* Jochen Wilhelmy <!-- 1 -->
* Joerg Schiermeier <!-- 1 -->
* John Schroeder <!-- 1 -->
* Jonathan L. Verner <!-- 1 -->
* Jonathan Raphael Joachim Kolberg <!-- 1 -->
* Jonathan Singer <!-- 1 -->
* José Joaquín Atria <!-- 1 -->
* Juan Francisco Cantero Hurtado <!-- 1 -->
* Juliano F. Ravasi <!-- 1 -->
* Juraj Oravec <!-- 1 -->
* Karol Szwed <!-- 1 -->
* Kevin Ballard <!-- 1 -->
* Kishore Gopalakrishnan <!-- 1 -->
* Kurt Granroth <!-- 1 -->
* Kurt Hindenburg <!-- 1 -->
* Kyle S Horne <!-- 1 -->
* Laurent Cimon <!-- 1 -->
* Lauri Watts <!-- 1 -->
* Lays Rodrigues <!-- 1 -->
* Leandro Emanuel López <!-- 1 -->
* Leandro Santiago <!-- 1 -->
* Li-yao Xia <!-- 1 -->
* Liu Zhe <!-- 1 -->
* Maciej Mrozowski <!-- 1 -->
* Magnus Hoff <!-- 1 -->
* Malte Starostik <!-- 1 -->
* Manuel Tortosa <!-- 1 -->
* Marc Espie <!-- 1 -->
* Marco Martin <!-- 1 -->
* Marijn Kruisselbrink <!-- 1 -->
* Mario Aichinger <!-- 1 -->
* Markus Brenneis <!-- 1 -->
* Martin Gräßlin <!-- 1 -->
* Martin Klapetek <!-- 1 -->
* Martin Sandsmark <!-- 1 -->
* Martin Tobias Holmedahl Sandsmark <!-- 1 -->
* Matheus C. França <!-- 1 -->
* Matt Carberry <!-- 1 -->
* Matthias Gerstner <!-- 1 -->
* Matthias Hoelzer-Kluepfel <!-- 1 -->
* Matthias Klumpp <!-- 1 -->
* Melchior Franz <!-- 1 -->
* Michael Drueing <!-- 1 -->
* Michael Heidelbach <!-- 1 -->
* Michael Matz <!-- 1 -->
* Michael Ritzert <!-- 1 -->
* Michal Srb <!-- 1 -->
* Mikhail Zolotukhin <!-- 1 -->
* Mikko Perttunen <!-- 1 -->
* Miquel Sabaté Solà <!-- 1 -->
* Momo Cao <!-- 1 -->
* Médéric Boquien <!-- 1 -->
* Nazar Kalinowski <!-- 1 -->
* Nico Kruber <!-- 1 -->
* Nicola Gigante <!-- 1 -->
* Nicolas Lécureuil <!-- 1 -->
* Nikolay Kultashev <!-- 1 -->
* Oleksandr Senkovych <!-- 1 -->
* Oliver Sander <!-- 1 -->
* Olivier CHURLAUD <!-- 1 -->
* Olivier Felt <!-- 1 -->
* Ovidiu-Florin BOGDAN <!-- 1 -->
* Parker Coates <!-- 1 -->
* Patrick José Pereira <!-- 1 -->
* Paul Gideon Dann <!-- 1 -->
* Paulo Barreto <!-- 1 -->
* Paulo Moura Guedes <!-- 1 -->
* Pavel Pertsev <!-- 1 -->
* Pedro Gimeno <!-- 1 -->
* Per Winkvist <!-- 1 -->
* Peter J. Mello <!-- 1 -->
* Peter Mello <!-- 1 -->
* Peter Penz <!-- 1 -->
* Philippe Fremy <!-- 1 -->
* Rafał Miłecki <!-- 1 -->
* Ralf Jung <!-- 1 -->
* Ralf Nolden <!-- 1 -->
* Ramon Zarazua <!-- 1 -->
* Randy Kron <!-- 1 -->
* Raphael Rosch <!-- 1 -->
* Richard Mader <!-- 1 -->
* Robert Gruber <!-- 1 -->
* Rohan Garg <!-- 1 -->
* Rolf Magnus <!-- 1 -->
* Roman Gilg <!-- 1 -->
* Ruediger Gad <!-- 1 -->
* Sandro Giessl <!-- 1 -->
* Sascha Cunz <!-- 1 -->
* Sean Gillespie <!-- 1 -->
* Shane Wright <!-- 1 -->
* Simone Scalabrino <!-- 1 -->
* Stefan Gehn <!-- 1 -->
* Steve Mokris <!-- 1 -->
* Sven Greb <!-- 1 -->
* Sven Lüppken <!-- 1 -->
* Thomas Capricelli <!-- 1 -->
* Thomas Horstmeyer <!-- 1 -->
* Thomas Jarosch <!-- 1 -->
* Thomas McGuire <!-- 1 -->
* Thomas Reitelbach <!-- 1 -->
* Till Schfer <!-- 1 -->
* Tim Jansen <!-- 1 -->
* Tom Albers <!-- 1 -->
* Tore Melangen Havn <!-- 1 -->
* Vladimír Vondruš <!-- 1 -->
* Werner Trobin <!-- 1 -->
* Wilco Greven <!-- 1 -->
* Will Stephenson <!-- 1 -->
* Wojciech Stachurski <!-- 1 -->
* Wouter Becq <!-- 1 -->
* Yuen Hoe Lim <!-- 1 -->
* Zhigalin Alexander <!-- 1 -->
* Zoe Clifford <!-- 1 -->
* aa bb <!-- 1 -->
* bombless <!-- 1 -->
* est 31 <!-- 1 -->
* gamazeps <!-- 1 -->
* m.eik michalke <!-- 1 -->
* mdinger <!-- 1 -->
* tfry <!-- 1 -->
* visualfc <!-- 1 -->
* Émeric Dupont <!-- 1 -->
* Ömer Fadıl Usta <!-- 1 -->
* Сковорода Никита Андреевич <!-- 1 -->
