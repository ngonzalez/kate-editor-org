---
author: Christoph Cullmann
date: 2010-07-09 14:40:05
title: "Obt\xE9n el Kate"
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux i Unixs

* Instal·la Kate/KWrite [des de la distribució](http://www.kde.org/download/distributions.php).

* [Construïu-lo](/build-it/#linux) a partir del codi font.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

* El [Kate a la Windows Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

* El [Kate via Chocolatey](https://chocolatey.org/packages/kate)

* L'[instal·lador del llançament del Kate (64bits)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/)

* L'[instal·lador de la versió nocturna del Kate (64bits)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/)

* [Construïu-lo](/build-it/#windows) a partir del codi font.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/OS_X_El_Capitan_logo.svg_.png" >}}

### macOS

* L'[instal·lador del llançament del Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/)

* L'[instal·lador de la versió nocturna del Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/)

* [Construïu-lo](/build-it/#mac) a partir del codi font.