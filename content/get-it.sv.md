---
author: Christoph Cullmann
date: 2010-07-09 14:40:05
title: "H\xE4mta Kate"
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux och Unix

* Installera Kate och KWrite [från din distribution](http://www.kde.org/download/distributions.php).

* [Bygg det](/build-it/#linux) från källkod.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

* [Kate på Windows Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

+ [Kate via Chocolatey](https://chocolatey.org/packages/kate)

* [Kates (64-bitars) installationsverktyg för utgåvor](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/)

* [Kates nattliga (64-bitars) installationsverktyg](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/)

* [Bygg det](/build-it/#windows) från källkod.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/OS_X_El_Capitan_logo.svg_.png" >}}

### macOS

* [Kates installationsverktyg för utgåvor](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/)

* [Kates nattliga installationsverktyg](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/)

* [Bygg det](/build-it/#mac) från källkod.