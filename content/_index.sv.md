---
layout: index
---
Kate är en editor för flera dokument som ingår i [KDE](https://kde.org) sedan utgåva 2.2. Eftersom det är ett [KDE-program](https://kde.org/applications), levereras Kate med nätverkstransparens, samt integrering med KDE:s enastående funktioner. Välj det för att titta på HTML-källkod från Konqueror, redigera inställningsfiler, skriva nya program eller för vilken annan textredigeringsuppgift som helst. Man behöver ändå bara en instans av Kate som kör. [Ta reda på mer...](/about/)

![Skärmbild av Kate som visar flera dokument och terminalemulatorn för
byggning](/images/kate-window.png)