#!/usr/bin/env bash

FILENAME="kate-editor-org"

function export_pot_file # First parameter will be the path of the pot file we have to create, includes $FILENAME
{
    potfile=$1
    python3 translations.py extract $potfile
}

function import_po_files # First parameter will be a path that will contain several .po files with the format LANG.po
{
    podir=$1
    python3 translations.py import $podir
    python3 translations.py generate-translations
    rm -rf locale
}

